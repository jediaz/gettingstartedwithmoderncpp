#include <iostream>
#include <sstream> // for std::ostringstream
#include <string>
#include <cmath> // for std::round


//! Returns `number` * (2 ^ `exponent`) 
template<class IntT>
IntT TimesPowerOf2(IntT number, int exponent)
{
    constexpr IntT two = static_cast<IntT>(2);    
    
    while (exponent > 0)
    { 
        number *= two; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= two;
        exponent += 1 ; 
    }
    
    return number;
}
    

//! Round to `x` the nearest integer.
template<class IntT>
IntT RoundAsInt(double x)
{
    return static_cast<IntT>(std::round(x));
}


// Maximum integer that might be represented with `Nbits` bits.
template<class IntT>
IntT MaxInt(int Nbits)
{ 
    constexpr IntT one = static_cast<IntT>(1);
    return (TimesPowerOf2(one, Nbits) - one);
}


//! Class to group the two integers used in the approximation we define.
template<class IntT>
class PowerOfTwoApprox
{
public:
    
    //! Compute the best possible approximation of `value` with `Nbits`
    PowerOfTwoApprox(int Nbits, double value);

    //! \return The approximation as a floating point.
    explicit operator double() const;
    
    //! Accessor to numerator.
    IntT GetNumerator() const;
    
    //! Accessor to exponent.
    int GetExponent() const;
    
 
        
private:
        
    IntT numerator_ {};
    int exponent_ {};    
};  


template<class IntT>
std::ostream& operator<<(std::ostream& out, const PowerOfTwoApprox<IntT>& approximation)
{
    out << approximation.GetNumerator() << "/2^" << approximation.GetExponent();
    return out;
}


/*! 
 * \brief Multiply the approximate representation by an integer. 
 * 
 * \param[in] coefficient Integer coefficient by which the object is multiplied.
 * 
 * \return An approximate integer result of the multiplication.
 */
template<class IntT>
IntT operator*(IntT coefficient, const PowerOfTwoApprox<IntT>& approx)
{
  return TimesPowerOf2(approx.GetNumerator() * coefficient, -approx.GetExponent());
}
 

template<class IntT> 
IntT operator*(const PowerOfTwoApprox<IntT>& approx, IntT coefficient)
{
  return coefficient * approx;
}


//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! Note: could be put in the struct... but may be kept a free function as well! We will see much later
//! the anonymous namespace, in which I would typically put such a free function.
template<class IntT>
void HelperComputePowerOf2Approx(double value, int exponent, IntT& numerator, IntT& denominator)
{
    constexpr IntT one = static_cast<IntT>(1);
    denominator = TimesPowerOf2(one, exponent);   
    numerator = RoundAsInt<IntT>(value * denominator);
}


template<class IntT>
PowerOfTwoApprox<IntT>::PowerOfTwoApprox(int Nbits, double value)
{
    IntT max_numerator = MaxInt<IntT>(Nbits);
    
    auto& numerator = numerator_;
    int& exponent = exponent_;    
    
    numerator = 0;
    exponent = 0;
    IntT denominator {};
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperComputePowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    HelperComputePowerOf2Approx(value, --exponent, numerator, denominator);
}


template<class IntT>
PowerOfTwoApprox<IntT>::operator double() const
{
    constexpr IntT one = static_cast<IntT>(1);
    IntT denominator = TimesPowerOf2(one, exponent_);
    return static_cast<double>(numerator_) / denominator;
}


template<class IntT>
IntT PowerOfTwoApprox<IntT>::GetNumerator() const
{
    return numerator_;
}


template<class IntT>
int PowerOfTwoApprox<IntT>::GetExponent() const
{
    return exponent_;
}


enum class RoundToInteger { no, yes };


class TestDisplay
{
public:
    
    //! Constructor which sets the only the resolution (i.e. the maximum index upon which error is defined).
    TestDisplay(int resolution);
    
    //! To make TestDisplay an abstract class.
    virtual ~TestDisplay();
    
    //! Pure virtual method Do().
    virtual void operator()(int Nbits) const = 0;


protected:
    
   /*!
    * \brief  Print a line with information about error.
    *
    * \param[in] optional_string1 String that might appear just after the "[With N bits]:".
    * \param[in] optional_string2 String that might appear just before the "[error = ...]".
    * \param[in] do_round_to_integer If yes, the exact result is approximated as an integer.
    */
    template<class IntT>
   void PrintLine(int Nbits, double exact, double approx,
                  std::string optional_string1 = "", std::string optional_string2 = "",
                  RoundToInteger do_round_to_integer = RoundToInteger::no) const;
                  
private:
    
    //! Resolution.
    const int resolution_;
  
};


TestDisplay::TestDisplay(int resolution)
: resolution_(resolution)
{ }


TestDisplay::~TestDisplay() = default;


void TestDisplay::operator()(int Nbits) const
{
    static_cast<void>(Nbits); // neutralize warning about unused argument at no runtime cost.
}


template<class IntT>
void TestDisplay::PrintLine(int Nbits, double exact, double approx,
                            std::string optional_string1, std::string optional_string2,
                            RoundToInteger do_round_to_integer) const
{
    IntT error = RoundAsInt<IntT>(resolution_ * std::fabs(exact - approx) / exact);
 
    std::cout << "[With " << Nbits << " bits]: " << optional_string1
        << (do_round_to_integer == RoundToInteger::yes ? RoundAsInt<IntT>(exact) : exact)  << " ~ " << approx
        << optional_string2
        << "  [error = " << error << "/" << resolution_ << "]" 
        << std::endl;    
}


class TestDisplayPowerOfTwoApprox : public TestDisplay
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox(int resolution);
    
    //! Destructor
    virtual ~TestDisplayPowerOfTwoApprox() override;
    
    //! Pure virtual method Do().
    virtual void operator()(int Nbits)const override = 0;
    
protected:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value) const;
    
};


TestDisplayPowerOfTwoApprox::TestDisplayPowerOfTwoApprox(int resolution)
: TestDisplay(resolution)
{ }    

TestDisplayPowerOfTwoApprox::~TestDisplayPowerOfTwoApprox() = default;


void TestDisplayPowerOfTwoApprox::Display(int Nbits, double value) const
{
    using integer_type = long;
    PowerOfTwoApprox<integer_type> approximation(Nbits, value);

    const double approx = static_cast<double>(approximation);
    
    std::ostringstream oconv;
    oconv << "  (" << approximation << ")";    
    PrintLine<integer_type>(Nbits, value, approx, "", oconv.str());
}


class TestDisplayPowerOfTwoApprox065 : public TestDisplayPowerOfTwoApprox
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox065(int resolution);
    
    //! Destructor,
    ~TestDisplayPowerOfTwoApprox065() override;
    
     //! Display the output for the chosen `Nbits`.
    void operator()(int Nbits)const override;

};


TestDisplayPowerOfTwoApprox065::TestDisplayPowerOfTwoApprox065(int resolution)
: TestDisplayPowerOfTwoApprox(resolution)
{ }


TestDisplayPowerOfTwoApprox065::~TestDisplayPowerOfTwoApprox065() = default;


void TestDisplayPowerOfTwoApprox065::operator()(int Nbits) const
{
    Display(Nbits, 0.65);    
}


class TestDisplayPowerOfTwoApprox035 : public TestDisplayPowerOfTwoApprox
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox035(int resolution);
    
    //! Destructor,
    ~TestDisplayPowerOfTwoApprox035() override;
    
     //! Display the output for the chosen `Nbits`.
    void operator()(int Nbits)const override;

};


TestDisplayPowerOfTwoApprox035::TestDisplayPowerOfTwoApprox035(int resolution)
: TestDisplayPowerOfTwoApprox(resolution)
{ }

TestDisplayPowerOfTwoApprox035::~TestDisplayPowerOfTwoApprox035() = default;


void TestDisplayPowerOfTwoApprox035::operator()(int Nbits) const
{
    Display(Nbits, 0.35);    
}


template<class IntT>
class TestDisplaySumOfMultiply : public TestDisplay
{
public:
    
    //! Constructor.
    explicit TestDisplaySumOfMultiply(int resolution);
    
    //! To make the class a concrete one.
    virtual ~TestDisplaySumOfMultiply() override;
    
    
    //! Display the output for the chosen `Nbits`.
    void operator()(int Nbits)const override;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value1, IntT coefficient1, double value2, IntT coefficient2) const;
    
};


template<class IntT>
TestDisplaySumOfMultiply<IntT>::TestDisplaySumOfMultiply(int resolution)
: TestDisplay(resolution)
{ }


template<class IntT>
TestDisplaySumOfMultiply<IntT>::~TestDisplaySumOfMultiply() = default;


template<class IntT>
void TestDisplaySumOfMultiply<IntT>::operator()(int Nbits) const
{
    Display(Nbits, 0.65, static_cast<IntT>(3515), 0.35, static_cast<IntT>(4832));
}


template<class IntT>
void TestDisplaySumOfMultiply<IntT>::Display(int Nbits, double value1, IntT coefficient1, double value2, IntT coefficient2) const
{
    double exact = value1 * coefficient1 + value2 * coefficient2;
    
    PowerOfTwoApprox<IntT> approximation1(Nbits, value1);
    PowerOfTwoApprox<IntT> approximation2(Nbits, value2);    
    
    IntT approx = approximation1 * coefficient1  + coefficient2 * approximation2;
    
    std::ostringstream oconv;
    oconv << value1 << " * " << coefficient1 << " + " << value2 << " * " << coefficient2 << " = ";
    
    PrintLine<IntT>(Nbits, exact, static_cast<double>(approx), oconv.str(), "", RoundToInteger::yes);
}


//! Function for error handling. We will see later how to fulfill the same functionality more properly.
[[noreturn]] void Error(std::string explanation)
{
    std::cout << "ERROR: " << explanation << std::endl;
    exit(EXIT_FAILURE);
}


class TestDisplayContainer
{
public:
    
    //! Constructor.
    TestDisplayContainer(std::size_t capacity);
    
    //! Destructor.
    ~TestDisplayContainer();

    //! Add a new test_display_register.
    //! At each call, the item to be registered is put at the first available position and internal current_position_
    //! is incremented. If the end-user attempts to register more than three items, the error() function is called.
    void Register(TestDisplay* test_display);
    
    //! Accessor to the i-th object in the container.
    const TestDisplay& operator[](std::size_t i) const;
    
    //! Get the number of elements actually stored in the class (nullptr don't count).
    std::size_t GetSize() const;
    
    
private:
    
    //! Get the maximal number of elements that might be stored in the container.
    std::size_t GetCapacity() const;
    
private:    
    
    //! Maximum number of items that might be registered in the container.
    const std::size_t capacity_;
    
    //! List of all known `TestDisplay` objects.
    TestDisplay** list_;
    
    //! Index to place the next register object. If '3', no more object may be registered.
    std::size_t current_position_ {};

};


TestDisplayContainer::TestDisplayContainer(std::size_t capacity)
: capacity_(capacity)
{
    list_ = new TestDisplay*[capacity];
    
    for (auto i = 0ul; i < capacity; ++i)
        list_[i] = nullptr;
}


TestDisplayContainer::~TestDisplayContainer()
{
    for (auto i = 0ul; i < capacity_; ++i)
        delete list_[i];
    
    delete[] list_; // don't forget the [] to delete an array!
}


void TestDisplayContainer::Register(TestDisplay* test_display)
{
    if (current_position_ >= capacity_)
        Error("TestDisplayContainer is already full; impossible to register a new element!");
    
    list_[current_position_] = test_display;
    ++current_position_;
}


const TestDisplay& TestDisplayContainer::operator[](std::size_t i) const
{
    if (i >= GetCapacity())
        Error("You try to access an element out of bounds!");
    
    if (list_[i] == nullptr) // equivalent to i >= GetSize()
        Error("You try to access an element that was never initialized!");
    
    return *list_[i];    
}


std::size_t TestDisplayContainer::GetCapacity() const
{
    return capacity_;
}


std::size_t TestDisplayContainer::GetSize() const
{
    return current_position_;
}


//! For each container stored, loop oover all those bits and print the result on screen.
void Loop(int initial_Nbit, int final_Nbit, int increment_Nbit, const TestDisplayContainer& container)
{
    for (auto i = 0ul; i < container.GetSize(); ++i)
    {
        decltype(auto) current_test_display = container[i];
            
        for (int nbits = initial_Nbit; nbits <= final_Nbit; nbits += increment_Nbit)
            current_test_display(nbits);
        std::cout << std::endl;
    }
}


/************************************/
// Main function
/************************************/

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother      

    TestDisplayContainer container(3);
    
    using integer_type = long;
    
    container.Register(new TestDisplayPowerOfTwoApprox065(1000000));
    container.Register(new TestDisplayPowerOfTwoApprox035(1000000));
    container.Register(new TestDisplaySumOfMultiply<integer_type>(10000));
    
    Loop(4, 32, 4, container);
    
    return EXIT_SUCCESS;
}

