
template<class IntT>
void HelperComputePowerOf2Approx(double value, int exponent, IntT& numerator, IntT& denominator)
{
    constexpr IntT one = static_cast<IntT>(1);
    denominator = TimesPowerOf2(one, exponent);   
    numerator = RoundAsInt<IntT>(value * denominator);
}


template<class IntT>
PowerOfTwoApprox<IntT>::PowerOfTwoApprox(int Nbits, double value)
{
    IntT max_numerator = MaxInt<IntT>(Nbits);
    
    auto& numerator = numerator_;
    int& exponent = exponent_;    
    
    numerator = 0;
    exponent = 0;
    IntT denominator {};
    
    if (value < 0.)
    {
        sign_ = -1;
        value = -value;
    }
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperComputePowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    HelperComputePowerOf2Approx(value, --exponent, numerator, denominator);
}


template<class IntT>
PowerOfTwoApprox<IntT>::operator double() const
{
    constexpr IntT one = static_cast<IntT>(1);
    IntT denominator = TimesPowerOf2(one, exponent_);
    return static_cast<double>(numerator_) * sign_ / denominator;
}


template<class IntT>
IntT PowerOfTwoApprox<IntT>::GetNumerator() const
{
    return numerator_;
}


template<class IntT>
int PowerOfTwoApprox<IntT>::GetExponent() const
{
    return exponent_;
}


template<class IntT>
std::string PowerOfTwoApprox<IntT>::GetSignAsCharacter() const
{
    assert(sign_ == 1 || sign_ == -1);
    
    if (sign_ == 1)
        return "";
    else
        return "-";    
}


template<class IntT>
IntT PowerOfTwoApprox<IntT>::GetSignAsNumber() const
{
    return static_cast<IntT>(sign_);
}



template<class IntT>
std::ostream& operator<<(std::ostream& out, const PowerOfTwoApprox<IntT>& approximation)
{
    out << approximation.GetSignAsCharacter() << PrintInt(approximation.GetNumerator()) << "/2^" << approximation.GetExponent();
    return out;
}


template<class IntT>
IntT operator*(IntT coefficient, const PowerOfTwoApprox<IntT>& approx)
{
    IntT product;
    
    if (__builtin_mul_overflow(approx.GetNumerator(), coefficient, &product))
        throw Error("Overflow! (in operator*(IntT, const PowerOfTwoApprox<IntT>&))");

    return approx.GetSignAsNumber() * TimesPowerOf2(product, -approx.GetExponent());
}
 

template<class IntT> 
IntT operator*(const PowerOfTwoApprox<IntT>& approx, IntT coefficient)
{
    return coefficient * approx;
}
