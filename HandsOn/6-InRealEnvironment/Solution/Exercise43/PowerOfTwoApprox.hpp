#ifndef POWER_OF_TWO_APPROX_HPP
#define POWER_OF_TWO_APPROX_HPP

#include <cassert>
#include <iostream>
#include <string>

#include "Error.hpp"
#include "Tools.hpp"


//! Class to group the two integers used in the approximation we define.
template<class IntT>
class PowerOfTwoApprox
{
public:
    
    //! Compute the best possible approximation of `value` with `Nbits`
    PowerOfTwoApprox(int Nbits, double value);

    //! \return The approximation as a floating point.
    explicit operator double() const;
    
    //! Accessor to numerator.
    IntT GetNumerator() const;
    
    //! Accessor to exponent.
    int GetExponent() const;
    
    //! Sign as a character: either '-' or ''.
    // String might seem overkill, but empty char for positive case triggers a warnings.
    std::string GetSignAsCharacter() const;
    
    //! Sign as a number (e.g. 1 or -1).
    IntT GetSignAsNumber() const;
 
        
private:
        
    IntT numerator_ {};
    int exponent_ {};
    char sign_ { 1 };
};  


template<class IntT>
std::ostream& operator<<(std::ostream& out, const PowerOfTwoApprox<IntT>& approximation);


//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! Note: could be put in the struct... but may be kept a free function as well! We will see much later
//! the anonymous namespace, in which I would typically put such a free function.
template<class IntT>
void HelperComputePowerOf2Approx(double value, int exponent, IntT& numerator, IntT& denominator);



/*! 
 * \brief Multiply the approximate representation by an integer. 
 * 
 * \param[in] coefficient Integer coefficient by which the object is multiplied.
 * 
 * \return An approximate integer result of the multiplication.
 */
template<class IntT>
IntT operator*(IntT coefficient, const PowerOfTwoApprox<IntT>& approx);

template<class IntT> 
IntT operator*(const PowerOfTwoApprox<IntT>& approx, IntT coefficient);


#include "PowerOfTwoApprox.hxx"

#endif
