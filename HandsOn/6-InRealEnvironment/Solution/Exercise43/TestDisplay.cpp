#include <iostream>
#include "TestDisplay.hpp"



TestDisplay::TestDisplay(int resolution)
: resolution_(resolution)
{ }


TestDisplay::~TestDisplay() = default;


void TestDisplay::operator()(int Nbits) const
{
    static_cast<void>(Nbits); // neutralize warning about unused argument at no runtime cost.
}

void TestDisplay::PrintOverflow(int Nbits, const Error& e) const
{
    std::cout << "[With " << Nbits << " bits]: " << e.what() << std::endl;
}

