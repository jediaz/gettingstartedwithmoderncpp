// We use here the new "auto-to-stick" way to define functions here.
// Without it, we would have to use the much more verbosy
// template<class IntT>
// inline typename PrintIntHelper<IntT>::return_type PrintIntHelper<IntT>::Do(IntT value)
template<class IntT>
inline auto PrintIntHelper<IntT>::Do(IntT value)
-> return_type
{
    return static_cast<return_type>(value);
};


template<class T>
inline auto PrintInt(T value)
{
    return PrintIntHelper<T>::Do(value);
}


//! Returns `number` * (2 ^ `exponent`) 
template<class IntT>
IntT TimesPowerOf2(IntT number, int exponent)
{
    constexpr IntT two = static_cast<IntT>(2);
    
    while (exponent > 0)
    { 
        IntT product;
        
        if (__builtin_mul_overflow(number, two, &product))
        {
            std::cout << "OVERFLOW for number " << number << std::endl;
            throw Error("Overflow! (in TimesPowerOf2())");
        }

        number = product;
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= two;
        exponent += 1 ; 
    }
    
    return number;
}
    

template<class IntT>
inline IntT RoundAsInt(double x)
{
    return static_cast<IntT>(std::round(x));
}


template<class IntT>
inline IntT MaxInt(int Nbits)
{ 
    constexpr IntT one = static_cast<IntT>(1);
    return (TimesPowerOf2(one, Nbits) - one);
}
