#ifndef TEST_DISPLAY_HPP
#define TEST_DISPLAY_HPP

#include <string>
#include <sstream>

#include "PowerOfTwoApprox.hpp"
#include "Tools.hpp"

class TestDisplay
{
public:
    
    //! Constructor which sets the only the resolution (i.e. the maximum index upon which error is defined).
    TestDisplay(int resolution);
    
    //! To make TestDisplay an abstract class.
    virtual ~TestDisplay();
    
    //! Pure virtual method Do().
    virtual void operator()(int Nbits) const = 0;


protected:
    
   /*!
    * \brief  Print a line with information about error.
    *
    * \param[in] optional_string1 String that might appear just after the "[With N bits]:".
    * \param[in] optional_string2 String that might appear just before the "[error = ...]".
    * \param[in] do_round_to_integer If yes, the exact result is approximated as an integer.
    */
    template<class IntT>
    void PrintLine(int Nbits, double exact, double approx,
                   std::string optional_string1 = "", std::string optional_string2 = "",
                   RoundToInteger do_round_to_integer = RoundToInteger::no) const;
                   
    //! Function to call when an overflow occurred.
    void PrintOverflow(int Nbits, const Error& e) const;

                  
private:
    
    //! Resolution.
    const int resolution_;
  
};




template<class IntT>
class TestDisplayPowerOfTwoApprox : public TestDisplay
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox(int resolution);
    
    //! Destructor
    virtual ~TestDisplayPowerOfTwoApprox() override;
    
    //! Pure virtual method Do().
    virtual void operator()(int Nbits)const override = 0;
    
protected:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value) const;
    
};



template<class IntT>
class TestDisplayPowerOfTwoApproxMinus065 : public TestDisplayPowerOfTwoApprox<IntT>
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApproxMinus065(int resolution);
    
    //! Destructor,
    ~TestDisplayPowerOfTwoApproxMinus065() override;
    
     //! Display the output for the chosen `Nbits`.
    void operator()(int Nbits)const override;

};



template<class IntT>
class TestDisplayPowerOfTwoApprox035 : public TestDisplayPowerOfTwoApprox<IntT>
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox035(int resolution);
    
    //! Destructor,
    ~TestDisplayPowerOfTwoApprox035() override;
    
     //! Display the output for the chosen `Nbits`.
    void operator()(int Nbits)const override;

};



template<class IntT>
class TestDisplaySumOfMultiply : public TestDisplay
{
public:
    
    //! Constructor.
    explicit TestDisplaySumOfMultiply(int resolution);
    
    //! To make the class a concrete one.
    virtual ~TestDisplaySumOfMultiply() override;
    
    
    //! Display the output for the chosen `Nbits`.
    void operator()(int Nbits)const override;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value1, IntT coefficient1, double value2, IntT coefficient2) const;
    
};

#include "TestDisplay.hxx"

#endif
