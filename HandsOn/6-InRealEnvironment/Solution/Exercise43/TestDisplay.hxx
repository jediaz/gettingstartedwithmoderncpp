

template<class IntT>
void TestDisplay::PrintLine(int Nbits, double exact, double approx,
                            std::string optional_string1, std::string optional_string2,
                            RoundToInteger do_round_to_integer) const
{
    IntT error = RoundAsInt<IntT>(resolution_ * std::fabs(exact - approx) / std::fabs(exact));
 
    std::cout << "[With " << Nbits << " bits]: " << optional_string1
        << (do_round_to_integer == RoundToInteger::yes ? RoundAsInt<IntT>(exact) : exact)  << " ~ " << approx
        << optional_string2
        << "  [error = " << PrintInt(error) << "/" << resolution_ << "]" 
        << std::endl;    
}




template<class IntT>
TestDisplayPowerOfTwoApprox<IntT>::TestDisplayPowerOfTwoApprox(int resolution)
: TestDisplay(resolution)
{ }    


template<class IntT>
TestDisplayPowerOfTwoApprox<IntT>::~TestDisplayPowerOfTwoApprox() = default;


template<class IntT>
void TestDisplayPowerOfTwoApprox<IntT>::Display(int Nbits, double value) const
{
    std::ostringstream oconv;
    
    try
    {
        PowerOfTwoApprox<IntT> approximation(Nbits, value);

        const double approx = static_cast<double>(approximation);
            
        oconv << "  (" << approximation << ")";    
        PrintLine<IntT>(Nbits, value, approx, "", oconv.str());
    }
    catch(const Error& e)
    {
        PrintOverflow(Nbits, e);
    }
}




template<class IntT>
TestDisplayPowerOfTwoApproxMinus065<IntT>::TestDisplayPowerOfTwoApproxMinus065(int resolution)
: TestDisplayPowerOfTwoApprox<IntT>(resolution)
{ }


template<class IntT>
TestDisplayPowerOfTwoApproxMinus065<IntT>::~TestDisplayPowerOfTwoApproxMinus065() = default;


template<class IntT>
void TestDisplayPowerOfTwoApproxMinus065<IntT>::operator()(int Nbits) const
{
    this->Display(Nbits, -0.65);    
}



template<class IntT>
TestDisplayPowerOfTwoApprox035<IntT>::TestDisplayPowerOfTwoApprox035(int resolution)
: TestDisplayPowerOfTwoApprox<IntT>(resolution)
{ }


template<class IntT>
TestDisplayPowerOfTwoApprox035<IntT>::~TestDisplayPowerOfTwoApprox035() = default;


template<class IntT>
void TestDisplayPowerOfTwoApprox035<IntT>::operator()(int Nbits) const
{
    this->Display(Nbits, 0.35);    
}


template<class IntT>
TestDisplaySumOfMultiply<IntT>::TestDisplaySumOfMultiply(int resolution)
: TestDisplay(resolution)
{ }


template<class IntT>
TestDisplaySumOfMultiply<IntT>::~TestDisplaySumOfMultiply() = default;


template<class IntT>
void TestDisplaySumOfMultiply<IntT>::operator()(int Nbits) const
{
    Display(Nbits, -0.65, static_cast<IntT>(3515), 0.35, static_cast<IntT>(4832));
}


template<class IntT>
void TestDisplaySumOfMultiply<IntT>::Display(int Nbits, double value1, IntT coefficient1, double value2, IntT coefficient2) const
{
    try
    {
        double exact = value1 * coefficient1 + value2 * coefficient2;
    
        PowerOfTwoApprox<IntT> approximation1(Nbits, value1);
        PowerOfTwoApprox<IntT> approximation2(Nbits, value2);
    
        IntT part1 = approximation1 * coefficient1;
        IntT part2 = approximation2 * coefficient2;
        
        std::ostringstream oconv;
        oconv << value1 << " * " << coefficient1 << " + " << value2 << " * " << coefficient2 << " = ";

        IntT approx;
        
        if (__builtin_add_overflow(part1, part2, &approx))
            throw Error("Overflow (in TestDisplaySumOfMultiply<IntT>::Display())!");      
    
        PrintLine<IntT>(Nbits, exact, static_cast<double>(approx), oconv.str(), "", RoundToInteger::yes);
    }
    catch(const Error& e)
    {
        PrintOverflow(Nbits, e);
    }
}

