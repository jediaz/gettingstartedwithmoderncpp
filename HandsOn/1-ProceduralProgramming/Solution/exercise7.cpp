#include <iostream>
#include <cmath> // for std::round


//! Returns `number` * (2 ^ `exponent`) 
int TimesPowerOf2(int number, int exponent)
{
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    

//! Round to `x` the nearest integer.
int RoundAsInt(double x)
{
    return static_cast<int>(std::round(x));
}


// Maximum integer that might be represented with `Nbits` bits.
int MaxInt(int Nbits)
{ 
    return (TimesPowerOf2(1, Nbits) - 1);
}


//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
void HelperComputePowerOf2Approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = TimesPowerOf2(1, exponent);   
    numerator = RoundAsInt(value * denominator);
}



//! Compute the best possible approximation of `value` with `Nbits`
//! \param[out] numerator Computed numerator.
//! \param[out] exponent Computed exponent.
//! \return The approximation as a floating point.
double ComputePowerOf2Approx(int Nbits, double value, int& numerator, int& exponent)
{
    int max_numerator = MaxInt(Nbits);
    numerator = 0;
    exponent = 0;
    int denominator {};
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperComputePowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    HelperComputePowerOf2Approx(value, --exponent, numerator, denominator);
    
    return static_cast<double>(numerator) / denominator;     
}


//! Display the approximation of the given argument that fits in the given number of bits.
void DisplayPowerOf2Approx(int Nbits, double value)
{
    int numerator {}, exponent {};
    const double approx = ComputePowerOf2Approx(Nbits, value, numerator, exponent);
    
    const double error = std::fabs(value - approx) / value;
        
    std::cout << "[With " << Nbits << " bits]: " << value << " ~ " << approx << " (" << numerator << 
        " / 2^" << exponent << ")  [error = " << RoundAsInt(100. * error) << "/100]" << std::endl;
}



/************************************/
// Main function
/************************************/

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother 
        
    for (int nbits = 2; nbits <= 8; nbits += 2)
        DisplayPowerOf2Approx(nbits, 0.65) ;
    
    std::cout << std::endl;

    for (int nbits = 2; nbits <= 8; nbits += 2)
        DisplayPowerOf2Approx(nbits, 0.35) ;

    return EXIT_SUCCESS;
}

