#include <iostream>
#include <fstream>
#include <cmath> // for std::round


//! Returns `number` * (2 ^ `exponent`) 
int TimesPowerOf2(int number, int exponent)
{
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    

//! Round to `x` the nearest integer.
int RoundAsInt(double x)
{
    return static_cast<int>(std::round(x));
}


// Maximum integer that might be represented with `Nbits` bits.
int MaxInt(int Nbits)
{ 
    return (TimesPowerOf2(1, Nbits) - 1);
}


//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
void HelperComputePowerOf2Approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = TimesPowerOf2(1, exponent);   
    numerator = RoundAsInt(value * denominator);
}



//! Compute the best possible approximation of `value` with `Nbits`
//! \param[out] numerator Computed numerator.
//! \param[out] exponent Computed exponent.
//! \return The approximation as a floating point.
double ComputePowerOf2Approx(int Nbits, double value, int& numerator, int& exponent)
{
    int max_numerator = MaxInt(Nbits);
    numerator = 0;
    exponent = 0;
    int denominator {};
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperComputePowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    HelperComputePowerOf2Approx(value, --exponent, numerator, denominator);
    
    return static_cast<double>(numerator) / denominator;     
}


//! Display the approximation of the given argument that fits in the given number of bits.
void DisplayPowerOf2Approx(std::ostream& out, int Nbits, double value)
{
    int numerator {}, exponent {};
    const double approx = ComputePowerOf2Approx(Nbits, value, numerator, exponent);
    
    const double error = std::fabs(value - approx) / value;
        
    out << "[With " << Nbits << " bits]: " << value << " ~ " << approx << " (" << numerator << 
        " / 2^" << exponent << ")  [error = " << RoundAsInt(100. * error) << "/100]" << std::endl;
}



//! Multiply an approximated value by an integer.
int Multiply(int Nbits, double value, int coefficient)
{
    int numerator {}, exponent {};
    ComputePowerOf2Approx(Nbits, value, numerator, exponent);

    return TimesPowerOf2(numerator * coefficient, -exponent);
}


//! Compute value1 * coefficient1 + value2 * coefficient2 over Nbits bits.
void DisplaySumOfMultiply(std::ostream& out, int Nbits, double value1, int coefficient1, double value2, int coefficient2)
{
    double exact = value1 * coefficient1 + value2 * coefficient2;
    int rounded = RoundAsInt(exact);
    int approx = Multiply(Nbits, value1, coefficient1) + Multiply(Nbits, value2, coefficient2);
    
    const double error = std::fabs(exact - approx) / exact;
    
    out << "[With " << Nbits << " bits]: " << value1 << " * " << coefficient1 
        << " + " << value2 << " * " << coefficient2 << " = " 
        << rounded << " ~ " << approx 
        << "  [error = " << RoundAsInt(1000. * error) <<  "/1000]" <<  std::endl;    
}


void Display_065(std::ostream& out, int Nbits)
{ 
    DisplayPowerOf2Approx(out, Nbits, 0.65); 
}

void Display_035(std::ostream& out, int Nbits)
{
    DisplayPowerOf2Approx(out, Nbits, 0.35); 
}

void Display_065_3515_035_4832(std::ostream& out, int Nbits)
{ 
    DisplaySumOfMultiply(out, Nbits, 0.65, 3515, 0.35, 4832); 
}

void Loop(std::ostream& out, int initial, int final, int increment, void(*display_function)(std::ostream&, int))
{
    for (int bits = initial; bits <= final; bits += increment)
        display_function(out, bits);
    
    out << std::endl;
}


/************************************/
// Main function
/************************************/

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother 
        
    if (argc != 2)
       {
           std::cerr << "Two arguments are expected on command line: the name of the program followed by the "
               "path of the file in which some of the outputs will be written." << std::endl;
           exit(EXIT_FAILURE);
       }
    
   // Open the stream to the file.
   std::ofstream output_file(argv[1]);
    
   Loop(output_file, 2, 8, 2, Display_065);
   Loop(output_file, 2, 8, 2, Display_035);
   Loop(std::cout, 1, 8, 1, Display_065_3515_035_4832);

    return EXIT_SUCCESS;
}

