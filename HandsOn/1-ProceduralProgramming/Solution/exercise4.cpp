#include <iostream>
#include <cmath> // for std::round


//! Returns `number` * (2 ^ `exponent`) 
int TimesPowerOf2(int number, int exponent)
{
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    

//! Round to `x` the nearest integer.
int RoundAsInt(double x)
{
    return static_cast<int>(std::round(x));
}


//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
void HelperDisplayPowerOf2Approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = TimesPowerOf2(1, exponent);   
    numerator = RoundAsInt(value * denominator);
}


//! Display the approximation of the given argument that does not exceed the chosen maximum numerator.
void DisplayPowerOf2Approx(int max_numerator, double value)
{
    int numerator {}, denominator{}, exponent {};
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperDisplayPowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    HelperDisplayPowerOf2Approx(value, --exponent, numerator, denominator);
    
    double approx = static_cast<double>(numerator) / denominator; 
        
    std::cout << "[With numerator <= "<< max_numerator << "]: " << value << " ~ " << approx << " (" << numerator << 
        " / 2^" << exponent << ')' << std::endl;
}



/************************************/
// Main function
/************************************/

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother 

    DisplayPowerOf2Approx(15, 0.65);
    DisplayPowerOf2Approx(255, 0.65);

    DisplayPowerOf2Approx(15, 0.35);
    DisplayPowerOf2Approx(255, 0.35);
    
    return EXIT_SUCCESS;
}

