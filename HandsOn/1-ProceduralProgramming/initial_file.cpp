#include <iostream>
#include <cmath> // for std::round


//! Returns `number` * (2 ^ `exponent`) 
int TimesPowerOf2(int number, int exponent)
{
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    

//! Round to `x` the nearest integer.
int RoundAsInt(double x)
{
    return static_cast<int>(std::round(x));
}



/************************************/
// Main function
/************************************/

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother 
        
    int numerator1 = RoundAsInt(0.65 * TimesPowerOf2(1, 1));
    std::cout << "0.65 ~ " << numerator1 << " / 2^1" << std::endl ;
  
    int numerator2 = RoundAsInt(0.65 * TimesPowerOf2(1, 2));
    std::cout << "0.65 ~ " << numerator2 << " / 2^2" << std::endl ;
  
    int numerator3 = RoundAsInt(0.65 * TimesPowerOf2(1, 3));
    std::cout << "0.65 ~ " << numerator3 << " / 2^3" << std::endl ;
  
    std::cout << std::endl ;
    
    return EXIT_SUCCESS;
}

