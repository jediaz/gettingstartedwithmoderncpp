#include <iostream>
#include <sstream> // for std::ostringstream
#include <string>
#include <cmath> // for std::round
#include <algorithm>


//! I create here a new struct in charge of doing once and for all the conversion.
template<class IntT>
struct PrintIntHelper
{

    //! std::conditional_t is really close to the ternary operator except that it applies to types.
    using return_type = std::conditional_t
        <
            std::is_same<IntT, char>() || std::is_same<IntT, unsigned char>(),
            int,
            IntT
        >;
            
    static return_type Do(IntT value)
    {
        return static_cast<return_type>(value);
    } 

};


// From C++ 14 onward
// The function wasn't mandatory per se but allows to a more user-friendly interface:
// PrintInt(value);
// rather than
// PrintIntHelper<IntT>::Do(value);
template<class T>
auto PrintInt(T value)
{
    return PrintIntHelper<T>::Do(value);
}




class Error : public std::exception
{
public:
    
    //! Constructor.
    Error(const std::string& message);
    
    //! Overrides what method.
    virtual const char* what() const noexcept override;
    
private:
    
    const std::string message_;

};


Error::Error(const std::string& message)
: message_(message)
{ }


const char* Error::what() const noexcept
{
    return message_.c_str();
}


//! Returns `number` * (2 ^ `exponent`) 
template<class IntT>
IntT TimesPowerOf2(IntT number, int exponent)
{
    constexpr IntT two = static_cast<IntT>(2);
    
    while (exponent > 0)
    { 
        IntT product;
        
        if (__builtin_mul_overflow(number, two, &product))
            throw Error("Overflow! (in times_power_of_2())");

        number = product;
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= two;
        exponent += 1 ; 
    }
    
    return number;
}
    

//! Round to `x` the nearest integer.
template<class IntT>
IntT RoundAsInt(double x)
{
    return static_cast<IntT>(std::round(x));
}


// Maximum integer that might be represented with `Nbits` bits.
template<class IntT>
IntT MaxInt(int Nbits)
{ 
    constexpr IntT one = static_cast<IntT>(1);
    return (TimesPowerOf2(one, Nbits) - one);
}


//! Class to group the two integers used in the approximation we define.
template<class IntT>
class PowerOfTwoApprox
{
public:
    
    //! Compute the best possible approximation of `value` with `Nbits`
    PowerOfTwoApprox(int Nbits, double value);

    //! \return The approximation as a floating point.
    explicit operator double() const;
    
    //! Accessor to numerator.
    IntT GetNumerator() const;
    
    //! Accessor to exponent.
    int GetExponent() const;
    
 
        
private:
        
    IntT numerator_ {};
    int exponent_ {};    
};  


template<class IntT>
std::ostream& operator<<(std::ostream& out, const PowerOfTwoApprox<IntT>& approximation)
{
    out << PrintInt(approximation.GetNumerator()) << "/2^" << approximation.GetExponent();
    return out;
}


/*! 
 * \brief Multiply the approximate representation by an integer. 
 * 
 * \param[in] coefficient Integer coefficient by which the object is multiplied.
 * 
 * \return An approximate integer result of the multiplication.
 */
template<class IntT>
IntT operator*(IntT coefficient, const PowerOfTwoApprox<IntT>& approx)
{
    IntT product;
    
    if (__builtin_mul_overflow(approx.GetNumerator(), coefficient, &product))
        throw Error("Overflow! (in operator*(IntT, const PowerOfTwoApprox<IntT>&))");

    return TimesPowerOf2(product, -approx.GetExponent());
}
 

template<class IntT> 
IntT operator*(const PowerOfTwoApprox<IntT>& approx, IntT coefficient)
{
    return coefficient * approx;
}


//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! Note: could be put in the struct... but may be kept a free function as well! We will see much later
//! the anonymous namespace, in which I would typically put such a free function.
template<class IntT>
void HelperComputePowerOf2Approx(double value, int exponent, IntT& numerator, IntT& denominator)
{
    constexpr IntT one = static_cast<IntT>(1);
    denominator = TimesPowerOf2(one, exponent);   
    numerator = RoundAsInt<IntT>(value * denominator);
}


template<class IntT>
PowerOfTwoApprox<IntT>::PowerOfTwoApprox(int Nbits, double value)
{
    IntT max_numerator = MaxInt<IntT>(Nbits);
    
    auto& numerator = numerator_;
    int& exponent = exponent_;    
    
    numerator = 0;
    exponent = 0;
    IntT denominator {};
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperComputePowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    HelperComputePowerOf2Approx(value, --exponent, numerator, denominator);
}


template<class IntT>
PowerOfTwoApprox<IntT>::operator double() const
{
    constexpr IntT one = static_cast<IntT>(1);
    IntT denominator = TimesPowerOf2(one, exponent_);
    return static_cast<double>(numerator_) / denominator;
}


template<class IntT>
IntT PowerOfTwoApprox<IntT>::GetNumerator() const
{
    return numerator_;
}


template<class IntT>
int PowerOfTwoApprox<IntT>::GetExponent() const
{
    return exponent_;
}


enum class RoundToInteger { no, yes };


class TestDisplay
{
public:
    
    //! Constructor which sets the only the resolution (i.e. the maximum index upon which error is defined).
    TestDisplay(int resolution);
    
    //! To make TestDisplay an abstract class.
    virtual ~TestDisplay();
    
    //! Pure virtual method Do().
    virtual void operator()(int Nbits) const = 0;


protected:
    
   /*!
    * \brief  Print a line with information about error.
    *
    * \param[in] optional_string1 String that might appear just after the "[With N bits]:".
    * \param[in] optional_string2 String that might appear just before the "[error = ...]".
    * \param[in] do_round_to_integer If yes, the exact result is approximated as an integer.
    */
    template<class IntT>
    void PrintLine(int Nbits, double exact, double approx,
                   std::string optional_string1 = "", std::string optional_string2 = "",
                   RoundToInteger do_round_to_integer = RoundToInteger::no) const;
                   
    //! Function to call when an overflow occurred.
    void PrintOverflow(int Nbits, const Error& e) const;

                  
private:
    
    //! Resolution.
    const int resolution_;
  
};


TestDisplay::TestDisplay(int resolution)
: resolution_(resolution)
{ }


TestDisplay::~TestDisplay() = default;


void TestDisplay::operator()(int Nbits) const
{
    static_cast<void>(Nbits); // neutralize warning about unused argument at no runtime cost.
}


template<class IntT>
void TestDisplay::PrintLine(int Nbits, double exact, double approx,
                            std::string optional_string1, std::string optional_string2,
                            RoundToInteger do_round_to_integer) const
{
    IntT error = RoundAsInt<IntT>(resolution_ * std::fabs(exact - approx) / exact);
 
    std::cout << "[With " << Nbits << " bits]: " << optional_string1
        << (do_round_to_integer == RoundToInteger::yes ? RoundAsInt<IntT>(exact) : exact)  << " ~ " << approx
        << optional_string2
        << "  [error = " << PrintInt(error) << "/" << resolution_ << "]" 
        << std::endl;    
}


void TestDisplay::PrintOverflow(int Nbits, const Error& e) const
{
    std::cout << "[With " << Nbits << " bits]: " << e.what() << std::endl;
}


template<class IntT>
class TestDisplayPowerOfTwoApprox : public TestDisplay
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox(int resolution);
    
    //! Destructor
    virtual ~TestDisplayPowerOfTwoApprox() override;
    
    //! Pure virtual method Do().
    virtual void operator()(int Nbits)const override = 0;
    
protected:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value) const;
    
};


template<class IntT>
TestDisplayPowerOfTwoApprox<IntT>::TestDisplayPowerOfTwoApprox(int resolution)
: TestDisplay(resolution)
{ }    


template<class IntT>
TestDisplayPowerOfTwoApprox<IntT>::~TestDisplayPowerOfTwoApprox() = default;


template<class IntT>
void TestDisplayPowerOfTwoApprox<IntT>::Display(int Nbits, double value) const
{
    std::ostringstream oconv;
    
    try
    {
        PowerOfTwoApprox<IntT> approximation(Nbits, value);

        const double approx = static_cast<double>(approximation);
            
        oconv << "  (" << approximation << ")";    
        PrintLine<IntT>(Nbits, value, approx, "", oconv.str());
    }
    catch(const Error& e)
    {
        PrintOverflow(Nbits, e);
    }
}


template<class IntT>
class TestDisplayPowerOfTwoApprox065 : public TestDisplayPowerOfTwoApprox<IntT>
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox065(int resolution);
    
    //! Destructor,
    ~TestDisplayPowerOfTwoApprox065() override;
    
     //! Display the output for the chosen `Nbits`.
    void operator()(int Nbits)const override;

};


template<class IntT>
TestDisplayPowerOfTwoApprox065<IntT>::TestDisplayPowerOfTwoApprox065(int resolution)
: TestDisplayPowerOfTwoApprox<IntT>(resolution)
{ }


template<class IntT>
TestDisplayPowerOfTwoApprox065<IntT>::~TestDisplayPowerOfTwoApprox065() = default;


template<class IntT>
void TestDisplayPowerOfTwoApprox065<IntT>::operator()(int Nbits) const
{
    this->Display(Nbits, 0.65);    
}


template<class IntT>
class TestDisplayPowerOfTwoApprox035 : public TestDisplayPowerOfTwoApprox<IntT>
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox035(int resolution);
    
    //! Destructor,
    ~TestDisplayPowerOfTwoApprox035() override;
    
     //! Display the output for the chosen `Nbits`.
    void operator()(int Nbits)const override;

};


template<class IntT>
TestDisplayPowerOfTwoApprox035<IntT>::TestDisplayPowerOfTwoApprox035(int resolution)
: TestDisplayPowerOfTwoApprox<IntT>(resolution)
{ }


template<class IntT>
TestDisplayPowerOfTwoApprox035<IntT>::~TestDisplayPowerOfTwoApprox035() = default;


template<class IntT>
void TestDisplayPowerOfTwoApprox035<IntT>::operator()(int Nbits) const
{
    this->Display(Nbits, 0.35);    
}


template<class IntT>
class TestDisplaySumOfMultiply : public TestDisplay
{
public:
    
    //! Constructor.
    explicit TestDisplaySumOfMultiply(int resolution);
    
    //! To make the class a concrete one.
    virtual ~TestDisplaySumOfMultiply() override;
    
    
    //! Display the output for the chosen `Nbits`.
    void operator()(int Nbits)const override;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value1, IntT coefficient1, double value2, IntT coefficient2) const;
    
};


template<class IntT>
TestDisplaySumOfMultiply<IntT>::TestDisplaySumOfMultiply(int resolution)
: TestDisplay(resolution)
{ }


template<class IntT>
TestDisplaySumOfMultiply<IntT>::~TestDisplaySumOfMultiply() = default;


template<class IntT>
void TestDisplaySumOfMultiply<IntT>::operator()(int Nbits) const
{
    Display(Nbits, 0.65, static_cast<IntT>(3515), 0.35, static_cast<IntT>(4832));
}


template<class IntT>
void TestDisplaySumOfMultiply<IntT>::Display(int Nbits, double value1, IntT coefficient1, double value2, IntT coefficient2) const
{
    try
    {
        double exact = value1 * coefficient1 + value2 * coefficient2;
    
        PowerOfTwoApprox<IntT> approximation1(Nbits, value1);
        PowerOfTwoApprox<IntT> approximation2(Nbits, value2);
    
        IntT part1 = approximation1 * coefficient1;
        IntT part2 = approximation2 * coefficient2;
        
        std::ostringstream oconv;
        oconv << value1 << " * " << coefficient1 << " + " << value2 << " * " << coefficient2 << " = ";

        IntT approx;
        
        if (__builtin_add_overflow(part1, part2, &approx))
            throw Error("Overflow (in TestDisplaySumOfMultiply<IntT>::Display())!");      
    
        PrintLine<IntT>(Nbits, exact, static_cast<double>(approx), oconv.str(), "", RoundToInteger::yes);
    }
    catch(const Error& e)
    {
        PrintOverflow(Nbits, e);
    }
}


template<std::size_t CapacityN>
class TestDisplayContainer
{
public:
    
    //! Constructor.
    TestDisplayContainer();
    
    //! Destructor.
    ~TestDisplayContainer();

    //! Add a new test_display_register.
    //! At each call, the item to be registered is put at the first available position and internal current_position_
    //! is incremented. If the end-user attempts to register more than three items, the error() function is called.
    void Register(TestDisplay* test_display);
    
    //! Accessor to the i-th object in the container.
    const TestDisplay& operator[](std::size_t i) const;
    
    //! Get the number of elements actually stored in the class (nullptr don't count).
    std::size_t GetSize() const;
    
    
private:
    
    //! Get the maximal number of elements that might be stored in the container.
    constexpr std::size_t GetCapacity() const;
    
private:    
    
    //! List of all known `TestDisplay` objects.
    TestDisplay** list_;
    
    //! Index to place the next register object. If '3', no more object may be registered.
    std::size_t current_position_ {};

};


template<std::size_t CapacityN>
TestDisplayContainer<CapacityN>::TestDisplayContainer()
{
    list_ = new TestDisplay*[CapacityN];
    
    for (auto i = 0ul; i < CapacityN; ++i)
        list_[i] = nullptr;
}


template<std::size_t CapacityN>
TestDisplayContainer<CapacityN>::~TestDisplayContainer()
{
    for (auto i = 0ul; i < CapacityN; ++i)
        delete list_[i];
    
    delete[] list_; // don't forget the [] to delete an array!
}


template<std::size_t CapacityN>
void TestDisplayContainer<CapacityN>::Register(TestDisplay* test_display)
{
    if (current_position_ >= CapacityN)
        throw Error("TestDisplayContainer is already full; impossible to register a new element!");
    
    list_[current_position_] = test_display;
    ++current_position_;
}


template<std::size_t CapacityN>
const TestDisplay& TestDisplayContainer<CapacityN>::operator[](std::size_t i) const
{
    if (i >= GetCapacity())
        throw Error("You try to access an element out of bounds!");
    
    if (list_[i] == nullptr) // equivalent to i >= GetSize()
        throw Error("You try to access an element that was never initialized!");
    
    return *list_[i];    
}


template<std::size_t CapacityN>
constexpr std::size_t TestDisplayContainer<CapacityN>::GetCapacity() const
{
    return CapacityN;
}


template<std::size_t CapacityN>
std::size_t TestDisplayContainer<CapacityN>::GetSize() const
{
    return current_position_;
}


//! For each container stored, loop oover all those bits and print the result on screen.
template<std::size_t CapacityN>
void Loop(int initial_Nbit, int final_Nbit, int increment_Nbit, const TestDisplayContainer<CapacityN>& container)
{
    for (auto i = 0ul; i < container.GetSize(); ++i)
    {
        decltype(auto) current_test_display = container[i];
            
        for (int nbits = initial_Nbit; nbits <= final_Nbit; nbits += increment_Nbit)
            current_test_display(nbits);
        std::cout << std::endl;
    }
}


/************************************/
// Main function
/************************************/

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother      

    try
    {
        TestDisplayContainer<3> container;
    
        using integer_type = char;
    
        container.Register(new TestDisplayPowerOfTwoApprox065<integer_type>(100000000));
        container.Register(new TestDisplayPowerOfTwoApprox035<integer_type>(100000000));
        container.Register(new TestDisplaySumOfMultiply<integer_type>(1000000));
    
        Loop(4, 32, 4, container);
    }
    catch(const std::exception& e)
    {
        std::cerr << "An error occurred in the program: " << e.what() << std::endl;
    }
    
    return EXIT_SUCCESS;
}

