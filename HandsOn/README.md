The more natural to run the hands-ons is to set up your local environment with a compiler and a development environment (an IDE, git, etc...)

However if need be we prepared a Docker image which includes a very basic environment with the minimum required to be able to run the hands-ons.

The image is based on Fedora and is created through this [Dockerfile](../docker/Dockerfile.fedora). 

To run it type:

````
docker run -it -v $PWD:/home/formation/gettingstartedwithmoderncpp --cap-drop=all registry.gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/fedora_for_tp:latest
````

Most options have already been covered in the [main README](../README.md); the only new one is:

- `-it` which specifies the container must be run in interactive mode (i.e. you get a prompt to a terminal inside the Docker environment).

