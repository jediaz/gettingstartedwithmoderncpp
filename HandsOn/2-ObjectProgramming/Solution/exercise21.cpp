#include <iostream>
#include <sstream> // for std::ostringstream
#include <string>
#include <cmath> // for std::round


//! Returns `number` * (2 ^ `exponent`) 
int TimesPowerOf2(int number, int exponent)
{
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    

//! Round to `x` the nearest integer.
int RoundAsInt(double x)
{
    return static_cast<int>(std::round(x));
}


// Maximum integer that might be represented with `Nbits` bits.
int MaxInt(int Nbits)
{ 
    return (TimesPowerOf2(1, Nbits) - 1);
}


//! Class to group the two integers used in the approximation we define.
class PowerOfTwoApprox
{
public:
    
    //! Compute the best possible approximation of `value` with `Nbits`
    PowerOfTwoApprox(int Nbits, double value);

    //! \return The approximation as a floating point.
    double AsDouble() const;
    
    //! Accessor to numerator.
    int GetNumerator() const;
    
    //! Accessor to exponent.
    int GetExponent() const;
    
   /*! 
    * \brief Multiply the approximate representation by an integer. 
    * 
    * \param[in] coefficient Integer coefficient by which the object is multiplied.
    * 
    * \return An approximate integer result of the multiplication.
    */
    int Multiply(int coefficient) const;
        
private:
        
    int numerator_ {};
    int exponent_ {};    
};  



//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! Note: could be put in the struct... but may be kept a free function as well! We will see much later
//! the anonymous namespace, in which I would typically put such a free function.
void HelperComputePowerOf2Approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = TimesPowerOf2(1, exponent);   
    numerator = RoundAsInt(value * denominator);
}


PowerOfTwoApprox::PowerOfTwoApprox(int Nbits, double value)
{
    int max_numerator = MaxInt(Nbits);
    
    int& numerator = numerator_;
    int& exponent = exponent_;    
    
    numerator = 0;
    exponent = 0;
    int denominator {};
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperComputePowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    HelperComputePowerOf2Approx(value, --exponent, numerator, denominator);
}


double PowerOfTwoApprox::AsDouble() const
{
    int denominator = TimesPowerOf2(1, exponent_);
    return static_cast<double>(numerator_) / denominator;
}


int PowerOfTwoApprox::GetNumerator() const
{
    return numerator_;
}


int PowerOfTwoApprox::GetExponent() const
{
    return exponent_;
}


int PowerOfTwoApprox::Multiply(int coefficient) const
{
    return TimesPowerOf2(GetNumerator() * coefficient, -GetExponent());
}


enum class RoundToInteger { no, yes };


class TestDisplay
{
public:
    
    //! Constructor which sets the only the resolution (i.e. the maximum index upon which error is defined).
    TestDisplay(int resolution);
    
    //! To make TestDisplay an abstract class.
    virtual ~TestDisplay() = 0;


protected:
    
   /*!
    * \brief  Print a line with information about error.
    *
    * \param[in] optional_string1 String that might appear just after the "[With N bits]:".
    * \param[in] optional_string2 String that might appear just before the "[error = ...]".
    * \param[in] do_round_to_integer If yes, the exact result is approximated as an integer.
    */
   void PrintLine(int Nbits, double exact, double approx,
                  std::string optional_string1 = "", std::string optional_string2 = "",
                  RoundToInteger do_round_to_integer = RoundToInteger::no) const;
                  
private:
    
    //! Resolution.
    const int resolution_;
  
};


TestDisplay::TestDisplay(int resolution)
: resolution_(resolution)
{ }


TestDisplay::~TestDisplay() = default;


void TestDisplay::PrintLine(int Nbits, double exact, double approx,
                            std::string optional_string1, std::string optional_string2,
                            RoundToInteger do_round_to_integer) const
{
    int error = RoundAsInt(resolution_ * std::fabs(exact - approx) / exact);
 
    std::cout << "[With " << Nbits << " bits]: " << optional_string1
        << (do_round_to_integer == RoundToInteger::yes ? RoundAsInt(exact) : exact)  << " ~ " << approx
        << optional_string2
        << "  [error = " << error << "/" << resolution_ << "]" 
        << std::endl;    
}


class TestDisplayPowerOfTwoApprox : public TestDisplay
{
public:
    
    //! Constructor.
    TestDisplayPowerOfTwoApprox(int resolution);
    
    //! To make the class a concrete one.
    virtual ~TestDisplayPowerOfTwoApprox() override;
    
    //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value) const;
    
};


TestDisplayPowerOfTwoApprox::TestDisplayPowerOfTwoApprox(int resolution)
: TestDisplay(resolution)
{ }    

TestDisplayPowerOfTwoApprox::~TestDisplayPowerOfTwoApprox() = default;


void TestDisplayPowerOfTwoApprox::Do(int Nbits) const
{
    Display(Nbits, 0.65);
    Display(Nbits, 0.35);    
}


void TestDisplayPowerOfTwoApprox::Display(int Nbits, double value) const
{
    PowerOfTwoApprox approximation(Nbits, value);

    const double approx = approximation.AsDouble();
    
    std::ostringstream oconv;
    oconv << "  (" << approximation.GetNumerator() << "/2^" << approximation.GetExponent() << ")";    
    PrintLine(Nbits, value, approx, "", oconv.str());
}


class TestDisplaySumOfMultiply : public TestDisplay
{
public:
    
    //! Constructor.
    TestDisplaySumOfMultiply(int resolution);
    
    //! To make the class a concrete one.
    virtual ~TestDisplaySumOfMultiply() override;
    
    
    //! Display the output for the chosen `Nbits`.
    void Do(int Nbits) const;
        
private:
    
    //! Method in charge of the actual display.
    void Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const;
    
};


TestDisplaySumOfMultiply::TestDisplaySumOfMultiply(int resolution)
: TestDisplay(resolution)
{ }

TestDisplaySumOfMultiply::~TestDisplaySumOfMultiply() = default;


void TestDisplaySumOfMultiply::Do(int Nbits) const
{
    Display(Nbits, 0.65, 3515, 0.35, 4832);
}


void TestDisplaySumOfMultiply::Display(int Nbits, double value1, int coefficient1, double value2, int coefficient2) const
{
    double exact = value1 * coefficient1 + value2 * coefficient2;
    
    PowerOfTwoApprox approximation1(Nbits, value1);
    PowerOfTwoApprox approximation2(Nbits, value2);    
    
    int approx = approximation1.Multiply(coefficient1) + approximation2.Multiply(coefficient2);
    
    std::ostringstream oconv;
    oconv << value1 << " * " << coefficient1 << " + " << value2 << " * " << coefficient2 << " = ";
    
    PrintLine(Nbits, exact, static_cast<double>(approx), oconv.str(), "", RoundToInteger::yes);
}


/************************************/
// Main function
/************************************/

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother 
     
    TestDisplayPowerOfTwoApprox test_display_approx(100);
     
    for (int nbits = 2; nbits <= 8; nbits += 2)
         test_display_approx.Do(nbits); 

    std::cout << std::endl;

    TestDisplaySumOfMultiply test_display_multiply(1000);

    for (int nbits = 1; nbits <= 8; ++nbits)
        test_display_multiply.Do(nbits);

    return EXIT_SUCCESS;
}

