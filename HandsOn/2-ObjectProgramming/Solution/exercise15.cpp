#include <iostream>
#include <cmath> // for std::round


//! Returns `number` * (2 ^ `exponent`) 
int TimesPowerOf2(int number, int exponent)
{
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    

//! Round to `x` the nearest integer.
int RoundAsInt(double x)
{
    return static_cast<int>(std::round(x));
}


// Maximum integer that might be represented with `Nbits` bits.
int MaxInt(int Nbits)
{ 
    return (TimesPowerOf2(1, Nbits) - 1);
}


//! Structure to group the two integers used in the approximation we define.
struct PowerOfTwoApprox
{
    
    //! Compute the best possible approximation of `value` with `Nbits`
    PowerOfTwoApprox(int Nbits, double value);

    //! \return The approximation as a floating point.
    double AsDouble() const;
        
    int numerator_ {};
    int exponent_ {};    
};  



//! Helper function that computes numerator and denominator.
//! You're not obliged to use a function, but this way you enforce the Don't Repeat Yourself (DRY) principle!
//! Note: could be put in the struct... but may be kept a free function as well! We will see much later
//! the anonymous namespace, in which I would typically put such a free function.
void HelperComputePowerOf2Approx(double value, int exponent, int& numerator, int& denominator)
{
    denominator = TimesPowerOf2(1, exponent);   
    numerator = RoundAsInt(value * denominator);
}


PowerOfTwoApprox::PowerOfTwoApprox(int Nbits, double value)
{
    int max_numerator = MaxInt(Nbits);
    
    int& numerator = numerator_;
    int& exponent = exponent_;    
    
    numerator = 0;
    exponent = 0;
    int denominator {};
    
    do
    {
        // I used here the preffix increment '++exponent' but you may put it on a separate line if you're not 
        // comfortable with it.
        HelperComputePowerOf2Approx(value, ++exponent, numerator, denominator);
    }
    while (numerator <= max_numerator);
    
    HelperComputePowerOf2Approx(value, --exponent, numerator, denominator);
}


double PowerOfTwoApprox::AsDouble() const
{
    int denominator = TimesPowerOf2(1, exponent_);
    return static_cast<double>(numerator_) / denominator;
}


//! Display the approximation of the given argument that fits in the given number of bits.
void DisplayPowerOf2Approx(int Nbits, double value)
{
    PowerOfTwoApprox approximation(Nbits, value);

    const double approx = approximation.AsDouble();
    
    const double error = std::fabs(value - approx) / value;
        
    std::cout << "[With " << Nbits << " bits]: " << value << " ~ " << approx << " (" << approximation.numerator_ << 
        " / 2^" << approximation.exponent_ << ")  [error = " << RoundAsInt(100. * error) << "/100]" << std::endl;
}



//! Multiply an approximated value by an integer.
int Multiply(int Nbits, double value, int coefficient)
{
    PowerOfTwoApprox approximation(Nbits, value);

    return TimesPowerOf2(approximation.numerator_ * coefficient, -approximation.exponent_);
}


//! Compute value1 * coefficient1 + value2 * coefficient2 over Nbits bits.
void DisplaySumOfMultiply(int Nbits, double value1, int coefficient1, double value2, int coefficient2)
{
    double exact = value1 * coefficient1 + value2 * coefficient2;
    int rounded = RoundAsInt(exact);
    int approx = Multiply(Nbits, value1, coefficient1) + Multiply(Nbits, value2, coefficient2);
    
    const double error = std::fabs(exact - approx) / exact;
    
    std::cout << "[With " << Nbits << " bits]: " << value1 << " * " << coefficient1 
        << " + " << value2 << " * " << coefficient2 << " = " 
        << rounded << " ~ " << approx 
        << "  [error = " << RoundAsInt(1000. * error) <<  "/1000]" <<  std::endl;    
}


/************************************/
// Main function
/************************************/

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother 
        
    for (int nbits = 2; nbits <= 8; nbits += 2)
        DisplayPowerOf2Approx(nbits, 0.65);
    
    std::cout << std::endl;

    for (int nbits = 2; nbits <= 8; nbits += 2)
        DisplayPowerOf2Approx(nbits, 0.35);
    
    std::cout << std::endl;

    for (int nbits = 1; nbits <= 8; ++nbits)
        DisplaySumOfMultiply(nbits, 0.65, 3515, 0.35, 4832); // to compute 0.65 * 3515 + 0.35 * 4832
    

    return EXIT_SUCCESS;
}

