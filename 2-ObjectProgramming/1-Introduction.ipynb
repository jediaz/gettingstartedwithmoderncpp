{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](./) - [Object programming](./0-main.ipynb) - [Introduction to the concept of object](./1-Introduction.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Motivation\" data-toc-modified-id=\"Motivation-1\">Motivation</a></span></li><li><span><a href=\"#The-C-response:-the-struct\" data-toc-modified-id=\"The-C-response:-the-struct-2\">The C response: the <code>struct</code></a></span></li><li><span><a href=\"#Passing-a-struct-to-a-function\" data-toc-modified-id=\"Passing-a-struct-to-a-function-3\">Passing a struct to a function</a></span><ul class=\"toc-item\"><li><span><a href=\"#Pass-by-const-reference\" data-toc-modified-id=\"Pass-by-const-reference-3.1\">Pass-by-const-reference</a></span></li><li><span><a href=\"#Pass-by-pointer\" data-toc-modified-id=\"Pass-by-pointer-3.2\">Pass-by-pointer</a></span></li></ul></li><li><span><a href=\"#Initialization-of-objects\" data-toc-modified-id=\"Initialization-of-objects-4\">Initialization of objects</a></span></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Motivation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes, there are variables that are bound to be initialized and used together. Let's consider the coordinates of a vector in a three-dimensional space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "#include <cmath>  // For std::sqrt\n",
    "\n",
    "double Norm(double v_x, double v_y, double v_z) \n",
    "{ \n",
    "    return std::sqrt( v_x * v_x + v_y * v_y + v_z * v_z ); \n",
    "}\n",
    "\n",
    "{\n",
    "    double v1_x, v1_y, v1_z;\n",
    "    v1_x = 1.;\n",
    "    v1_y = 5.;\n",
    "    v1_z = -2.;\n",
    "\n",
    "    std::cout << Norm(v1_x, v1_y, v1_z) << std::endl;\n",
    "\n",
    "    double v2_x, v2_y, v2_z;\n",
    "    v2_x = 2.;\n",
    "    v2_y = 2.;\n",
    "    v2_z = 4.;\n",
    "\n",
    "    std::cout << Norm(v2_x, v2_y, v2_z) << std::endl;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The code above is completely oblivious of the close relationship between `x`, `y` and `z`, and for instance the `Norm` function takes three distinct arguments. \n",
    "\n",
    "This is not just an inconveniency: this can lead to mistake if there is an error in the variables passed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    double v1_x, v1_y, v1_z;\n",
    "    v1_x = 1.;\n",
    "    v1_y = 5.;\n",
    "    v1_z = -2.;\n",
    "    double v2_x, v2_y, v2_z;\n",
    "    v2_x = 2.;\n",
    "    v2_y = 2.;\n",
    "    v2_z = 4.;\n",
    "\n",
    "    const double norm1 = Norm(v1_x, v1_y, v2_z); // probably not what was intended, but the program \n",
    "                                                 // has no way to figure out something is fishy!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The C response: the `struct`\n",
    "\n",
    "C introduced the `struct` to be able to group nicely data together and limit the risk I exposed above:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Vector\n",
    "{\n",
    "    double x;\n",
    "    double y;\n",
    "    double z;    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "double Norm(Vector v)\n",
    "{\n",
    "    return std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z); \n",
    "}\n",
    "\n",
    "{\n",
    "    Vector v1;\n",
    "    v1.x = 1.;\n",
    "    v1.y = 5.;\n",
    "    v1.z = -2.;\n",
    "\n",
    "    std::cout << Norm(v1) << std::endl;\n",
    "\n",
    "    Vector v2;\n",
    "    v2.x = 2.;\n",
    "    v2.y = 2.;\n",
    "    v2.z = 4.;\n",
    "\n",
    "    std::cout << Norm(v2) << std::endl;    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Calling `Norm` is now both more elegant (only one argument) and less dangerous (I can't mix by mistake coordinates from different objects).\n",
    "\n",
    "Let's introduce at this point a bit of vocabulary:\n",
    "\n",
    "- `x`, `y` and `z` in the structure are called **member variables** or **data attributes** (often shorten as **attributes** even if in a class this is actually not completely proper). On a side note: some C++ purists will be adamant only **member variables** should be used; but I rather use **data attributes** which is the term preferred in many others object programming languages.\n",
    "- `Vector` is a **struct**, which is a somewhat simplified **class** (we will explain the difference when we'll introduce classes).\n",
    "- `v1` and `v2` are **objects**.\n",
    "\n",
    "Let's also highlight the `.` syntax which allows to access the attributes of an object (e.g `v1.x`).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The semicolon at the end of a `struct`\n",
    "This comes historically from the C, where a `struct` could be defined and initialized at the same time (or should - Xeus-cling doesn't manage it... As usual you may check a full-fledged compiler accepts it [@Coliru](http://coliru.stacked-crooked.com/a/3b77606ea8082485)):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Xeus-cling issue (at least circa September 2022)\n",
    "\n",
    "struct VectorAndInstantiate\n",
    "{\n",
    "    double x;\n",
    "    double y;\n",
    "    double z;    \n",
    "} v1; // Here the struct is declared and at the same time an object v1 is created\n",
    "\n",
    "v1.x = 1.;\n",
    "v1.y = 5.;\n",
    "v1.z = -2.;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is absolutely **not** encouraged in C++, but it may help you to remember always closing a `struct` (or later a `class`) with a semicolon."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Passing a struct to a function\n",
    "\n",
    "In the `norm` function above, we passed as argument an object of `Vector` type by value. When we introduced functions, we saw there were three ways to pass an argument:\n",
    "* By value.\n",
    "* By reference.\n",
    "* By pointers.\n",
    "\n",
    "I didn't mention there the copy cost of a pass-by-value: copying a plain old data (POD) type such as an `int` or a `double` is actually cheap, and is recommended over a reference. But the story is not the same for an object: the cost of copying the object in the case of a pass-by-value may actually be quite high (imagine if there were an array with thousands of double values inside for instance) - and that's supposing the object is copyable (but we're not quite ready yet to deal with [that aspect](../3-Operators/4-CanonicalForm.ipynb#Uncopyable-class)).\n",
    "\n",
    "### Pass-by-const-reference\n",
    "\n",
    "So most of the time it is advised to pass arguments by reference, often along a `const` qualifier if the object is not to be modified by the function:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "double NormWithoutCopy(const Vector& v)\n",
    "{\n",
    "    return std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z); \n",
    "}\n",
    "\n",
    "{\n",
    "    Vector v1;\n",
    "    v1.x = 1.;\n",
    "    v1.y = 5.;\n",
    "    v1.z = -2.;\n",
    "\n",
    "    std::cout << NormWithoutCopy(v1) << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Pass-by-pointer\n",
    "\n",
    "Of course, if for some reason you prefer to use pointers it is also possible:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "double Norm(const Vector* const v) // can keep the name here: no possible ambiguity\n",
    "{\n",
    "    return std::sqrt((*v).x * (*v).x + (*v).y * (*v).y + (*v).z * (*v).z); \n",
    "}\n",
    "\n",
    "{\n",
    "    Vector v1;\n",
    "    v1.x = 1.;\n",
    "    v1.y = 5.;\n",
    "    v1.z = -2.;\n",
    "\n",
    "    std::cout << Norm(&v1) << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is more than little verbosy, so a shortcut has been introduced; `->` means you dereference a pointer and then calls the attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "double NormWithPointerShortcut(const Vector* const v)\n",
    "{\n",
    "    return std::sqrt(v->x * v->x + v->y * v->y + v->z * v->z); \n",
    "}\n",
    "\n",
    "{\n",
    "    Vector v1;\n",
    "    v1.x = 1.;\n",
    "    v1.y = 5.;\n",
    "    v1.z = -2.;\n",
    "\n",
    "    std::cout << NormWithPointerShortcut(&v1) << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initialization of objects\n",
    "\n",
    "So far, we have improved the way the `Norm` function is called, but the initialization of a vector is still a bit tedious. Let's wrap up a function to ease that:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "void Init(Vector& v, double x, double y, double z) \n",
    "{\n",
    "    v.x = x;\n",
    "    v.y = y;\n",
    "    v.z = z;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Vector v1;\n",
    "    Init(v1, 1., 5., -2.);\n",
    "    std::cout << \"Norm = \" << Norm(v1) << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[© Copyright](../COPYRIGHT.md)   \n",
    "",
    ""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "165px"
   },
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
