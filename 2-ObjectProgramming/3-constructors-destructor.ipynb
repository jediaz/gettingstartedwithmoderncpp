{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](./) - [Object programming](./0-main.ipynb) - [(Base) constructors and destructor](./3-constructors-destructor.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Introduction-to-base-constructor\" data-toc-modified-id=\"Introduction-to-base-constructor-1\">Introduction to base constructor</a></span><ul class=\"toc-item\"><li><span><a href=\"#[WARNING]-How-to-call-a-constructor-without-argument\" data-toc-modified-id=\"[WARNING]-How-to-call-a-constructor-without-argument-1.1\"><strong>[WARNING]</strong> How to call a constructor without argument</a></span></li><li><span><a href=\"#&quot;Auto-to-stick&quot;-syntax-for-constructor-calls\" data-toc-modified-id=\"&quot;Auto-to-stick&quot;-syntax-for-constructor-calls-1.2\">\"Auto-to-stick\" syntax for constructor calls</a></span></li></ul></li><li><span><a href=\"#Initializing-a-reference-data-attribute\" data-toc-modified-id=\"Initializing-a-reference-data-attribute-2\">Initializing a reference data attribute</a></span></li><li><span><a href=\"#Delegating-constructor\" data-toc-modified-id=\"Delegating-constructor-3\">Delegating constructor</a></span></li><li><span><a href=\"#Default-constructor\" data-toc-modified-id=\"Default-constructor-4\">Default constructor</a></span></li><li><span><a href=\"#Good-practice:-provide-in-the-data-attribute-declaration-a-default-value\" data-toc-modified-id=\"Good-practice:-provide-in-the-data-attribute-declaration-a-default-value-5\">Good practice: provide in the data attribute declaration a default value</a></span></li><li><span><a href=\"#Good-practice:-define-the-attribute-in-the-same-order-they-are-defined\" data-toc-modified-id=\"Good-practice:-define-the-attribute-in-the-same-order-they-are-defined-6\">Good practice: define the attribute in the same order they are defined</a></span></li><li><span><a href=\"#Good-practice:-use-explicit-constructors-by-default\" data-toc-modified-id=\"Good-practice:-use-explicit-constructors-by-default-7\">Good practice: use <code>explicit</code> constructors by default</a></span></li><li><span><a href=\"#Destructor\" data-toc-modified-id=\"Destructor-8\">Destructor</a></span><ul class=\"toc-item\"><li><span><a href=\"#Default-destructor\" data-toc-modified-id=\"Default-destructor-8.1\">Default destructor</a></span></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction to base constructor\n",
    "\n",
    "In fact, our previous `Init()` function is meant to be realized through a dedicated method called a **constructor**. By convention, a constructor shares the name of the struct or class.\n",
    "\n",
    "Several constructors may be defined for a given class, provided there is no signature overlap."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Vector\n",
    "{\n",
    "    double x_;\n",
    "    double y_;    \n",
    "    double z_;\n",
    "    \n",
    "    Vector(); // Constructor\n",
    "    \n",
    "    Vector(double x, double y, double z); // Another constructor\n",
    "    \n",
    "    double Norm() const;\n",
    "};"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Data attributes may be initialized more efficiently with a special syntax shown below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vector::Vector(double x, double y, double z)\n",
    ": x_(x), // See the syntax here: `:` to introduce data attributes initialization,\n",
    "y_(y), // and commas to separate the different data attributes.\n",
    "z_(z)\n",
    "{ \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vector::Vector()\n",
    ": x_(0.), // braces may also be used since C++ 11... but not supported here by Xeus-cling.\n",
    "y_(0.),\n",
    "z_(0.)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "double Vector::Norm() const\n",
    "{\n",
    "    return std::sqrt(x_ * x_ + y_ * y_ + z_ * z_);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Vector v(5., 6., -4.2); // note the creation of an object with a constructor call.\n",
    "    std::cout << v.Norm() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### **[WARNING]** How to call a constructor without argument\n",
    "\n",
    "There is a technicality for constructor without arguments: they must be called **without** parenthesis (the reason is a possible confusion with a [functor](../3-Operators/5-Functors.ipynb) - see Item 6 of \\cite{Meyers2001} or [this blog post](https://www.fluentcpp.com/2018/01/30/most-vexing-parse/) if you want to learn more about the reasons of this):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Vector v; // no parenthesis here!\n",
    "    std::cout << v.Norm() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### \"Auto-to-stick\" syntax for constructor calls\n",
    "\n",
    "A way to avoid the mistake entirely is to call the so-called \"auto-to-stick\" alternate syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    auto v = Vector(5, 10, 15); // auto-to-stick syntax: a new perfectly fine way to declare an object.\n",
    "    std::cout << v.Norm() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This syntax completely removes the ambiguity: you **have to** keep the `()`, so the constructor with no arguments doesn't become a special case that is handled differently."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    auto v = Vector(); // auto-to-stick syntax\n",
    "    std::cout << v.Norm() << std::endl;    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This auto-to-stick syntax is not widely used, but is advised by some developers as a natural evolution of the syntax of the language (it is very akin to the [alternate syntax for functions](../1-ProceduralProgramming/4-Functions.ipynb#Alternate-function-syntax) we saw earlier). \n",
    "\n",
    "I advise you to read this very interesting [FluentCpp post](https://www.fluentcpp.com/2018/09/28/auto-stick-changing-style/) about this syntax which ponders about the relunctance we might have to embrace evolution of our languages - so the reading is of interest even for developers using other languages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initializing a reference data attribute\n",
    "\n",
    "We saw above a new syntax to initialize data attributes, but there is no harm defining the same as we did in our former `Init()` method. That is however not true when there is a reference data attribute, for which the reference must absolutely be defined with the new syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct First\n",
    "{\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Second\n",
    "{\n",
    "    \n",
    "    const First& first_;\n",
    "    \n",
    "    Second(const First& first);\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Second::Second(const First& first)\n",
    "{\n",
    "    first_ = first; // COMPILATION ERROR: can't initialize a reference data attribute here!    \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Second::Second(const First& first)\n",
    ": first_(first) // OK!\n",
    "{ } "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The reason for this behaviour is that the reference must be defined at the very moment the object is created, and in fact the body of a constructor is run _after_ the actual creation occurs. With the proper syntax, the data is properly initialized in the same time the object is created.\n",
    "\n",
    "\n",
    "## Delegating constructor\n",
    "\n",
    "Since C++ 11, it is possible to use a base constructor when defining another constructor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Vector2\n",
    "{\n",
    "    double x_, y_, z_;\n",
    "    \n",
    "    Vector2();\n",
    "    \n",
    "    Vector2(double x);\n",
    "    \n",
    "    Vector2(double x, double y, double z);\n",
    "    \n",
    "    void Print() const;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void Vector2::Print() const\n",
    "{\n",
    "    std::cout << \"(x, y, z) = (\" << x_ << \", \" << y_ << \", \" << z_ << \")\" << std::endl << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "Vector2::Vector2()\n",
    ": x_(-1.),\n",
    "y_(-1.),\n",
    "z_(-1.)\n",
    "{\n",
    "    std::cout << \"Calling Vector2 constructor with no arguments.\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vector2::Vector2(double x, double y, double z)\n",
    ": x_(x),\n",
    "y_(y),\n",
    "z_(z)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vector2::Vector2(double x)\n",
    ": Vector2() \n",
    "{ \n",
    "    x_ = x; // As the first constructor is assumed to build fully the object, you can't assign data attributes \n",
    "            // before the body of the constructor.\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    std::cout << \"Constructor with no argument:\" << std::endl;\n",
    "    Vector2 v1;\n",
    "    v1.Print();\n",
    "    \n",
    "    std::cout << \"Constructor with no delegation:\" << std::endl;\n",
    "    Vector2 v2(3., 7., 5.);\n",
    "    v2.Print();\n",
    "    \n",
    "    std::cout << \"Constructor that calls a delegate constructor:\" << std::endl;    \n",
    "    Vector2 v3(3.);\n",
    "    v3.Print();    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Default constructor\n",
    "\n",
    "If in a `class` or `struct` no constructor is defined, a default one is assumed: it takes no argument and sports an empty body.\n",
    "\n",
    "As soon as another constructor is defined, this default constructor no longer exists:\n",
    "   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct ClassWithoutConstructor\n",
    "{ };"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    ClassWithoutConstructor my_object;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct ClassWithConstructorWithArg\n",
    "{\n",
    "    ClassWithConstructorWithArg(int a);\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ClassWithConstructorWithArg::ClassWithConstructorWithArg(int a)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    ClassWithConstructorWithArg my_object; // COMPILATION ERROR!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This must seem messy at first sight, but doing otherwise would be nightmarish: if you define a very complex class that must be carefully initialized with well thought-out arguments, you do not want your end-user to bypass this with an inconsiderate call to a constructor without arguments!\n",
    "\n",
    "If you want to enable back constructor without arguments, you may do so by defining it explicitly. C++11 introduced a nice way to do so (provided you wish an empty body - if not define it explicitly yourself):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct ClassWithConstructorWithAndWithoutArg\n",
    "{\n",
    "    ClassWithConstructorWithAndWithoutArg() = default;\n",
    "        \n",
    "    ClassWithConstructorWithAndWithoutArg(int a);    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ClassWithConstructorWithAndWithoutArg::ClassWithConstructorWithAndWithoutArg(int a)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    ClassWithConstructorWithAndWithoutArg my_object; // OK!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Good practice: provide in the data attribute declaration a default value\n",
    "\n",
    "In a constructor, you are expected to initialize properly all the data attributes. You can't expect a default behaviour if you fail to do so:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct BadlyInitialized\n",
    "{\n",
    "    int a_;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    BadlyInitialized my_object;\n",
    "    std::cout << \"Undefined behaviour: no guarantee for the value of the data attribute!: \" << my_object.a_ << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You are therefore supposed to define explicitly all the data attributes in all of your constructors. It was easy to get trumped by this in C++98/03: if you added a new data attribute and forgot to initialize it in one of your constructor, you would have undefined behaviour that is one of the worst bug to track down! (as on your machine/architecture you may have a \"good\" behaviour haphazardly).\n",
    "\n",
    "Fortunately, C++ 11 introduced a mechanism I strongly recommend to provide a default value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct SafeClass\n",
    "{\n",
    "    int a_ { 5 }; // The default value is provided here in the class declaration.        \n",
    "    \n",
    "    SafeClass() = default;\n",
    "    \n",
    "    SafeClass(int new_value);\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SafeClass::SafeClass(int new_value)\n",
    ": a_(new_value)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    SafeClass no_Arg;\n",
    "    std::cout << \"If constructor doesn't change the value, default is used: \" << no_Arg.a_ << std::endl;\n",
    "    \n",
    "    \n",
    "    SafeClass modified(10);\n",
    "    std::cout << \"If constructor changes the value, choice is properly used: \" << modified.a_ << std::endl;\n",
    "   \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please notice doing so doesn't prevent you to use the efficient initialization of `a_` in the constructor with arguments: the values thus provided in the data attributes definitions are used only if the constructor doesn't supersede them.\n",
    "\n",
    "In the same spirit, if you get pointers as data attributes it is a good idea to set them by default to `nullptr`: this way you may check with an [`assert`](../5-UsefulConceptsAndSTL/1-ErrorHandling.ipynb#Assert) it has been correctly initialized before use."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Good practice: define the attribute in the same order they are declared\n",
    "\n",
    "Data attributes should be defined in the exact same order they are declared within the class. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct BadOrder\n",
    "{\n",
    "    BadOrder();\n",
    "    \n",
    "    int a_;\n",
    "    bool b_;\n",
    "};    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "BadOrder::BadOrder()\n",
    ": b_(false), // WRONG: a_ should be defined before b_, as it is the order in which they are declared in the class.\n",
    "a_(5)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unfortunately Xeus-cling doesn't issue the warning here, but I gather all normal compilers do (even the oldest versions of clang and gcc available on [Wandbox](https://wandbox.org/) do in the very least...).\n",
    "\n",
    "Most of the time it doesn't matter much, but if there are dependencies between attributes it might create havoc so take the habit to respect the ordering.\n",
    "\n",
    "Of course, you may not define explicitly a data attribute for which you provide a default value at declaration:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <string>\n",
    "\n",
    "struct OkOrder\n",
    "{\n",
    "    OkOrder(int a, float c);\n",
    "    \n",
    "    int a_ {0};\n",
    "    std::string b_ {\"default_text\"};    \n",
    "    float c_ {3.5f};\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "OkOrder::OkOrder(int a, float c)\n",
    ": a_(a),\n",
    "c_(c) // Ok!\n",
    "{ }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Good practice: use `explicit` constructors by default\n",
    "\n",
    "Let's study the following case:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct ClassWithIntConstructor\n",
    "{\n",
    "    ClassWithIntConstructor(int a);\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "ClassWithIntConstructor::ClassWithIntConstructor(int a)\n",
    "{\n",
    "    std::cout << \"Constructor called with argument \" << a << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    ClassWithIntConstructor my_object(5);\n",
    "    \n",
    "    my_object = 7; // Dubious but correct: assigning an integer!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So what happens here? In fact, the compiler implicitly convert the integer read into a `ClassWithIntConstructor` constructor with an integer argument...\n",
    "\n",
    "There are situations in which this might be deemed the right thing to do (none to my mind but I guess it depends on your programming style) but more often than not it's not what is intended and a good old compiler yell would be much preferable.\n",
    "\n",
    "To do so, in C++ 11 you must stick the keyword **explicit** in the declaration in front of the constructor. Personally I tend to always provide it to my constructors, following the likewise advice by \\cite{Meyers2015}.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct ClassWithExplicitIntConstructor\n",
    "{\n",
    "    explicit ClassWithExplicitIntConstructor(int a);\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "ClassWithExplicitIntConstructor::ClassWithExplicitIntConstructor(int a)\n",
    "{\n",
    "    std::cout << \"Constructor called with argument \" << a << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    ClassWithExplicitIntConstructor my_object(5);\n",
    "    \n",
    "    my_object = 7; // COMPILATION ERROR! YAY!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true,
    "toc-nb-collapsed": true
   },
   "source": [
    "## Destructor\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The pendant of the constructor is the **destructor**, which is called when the object is terminated. Contrary to constructors, there is only one destructor for a given class, and by design it takes no parameter.\n",
    "\n",
    "The syntax is like a constructor with no parameter with an additional `~` in front of the name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Array\n",
    "{\n",
    "    Array(int unique_id, std::size_t array_size);\n",
    "    \n",
    "    ~Array(); // Destructor!    \n",
    "    \n",
    "    double* array_ = nullptr;\n",
    "    const int unique_id_;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Array::Array(int unique_id, std::size_t array_size)\n",
    ": unique_id_(unique_id)\n",
    "{\n",
    "    array_ = new double[array_size];\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "Array::~Array()\n",
    "{\n",
    "    std::cout << \"Memory for array \" << unique_id_ << \" is properly freed here.\" << std::endl;\n",
    "    delete[] array_;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Array array1(1, 5ul);\n",
    "    \n",
    "    {\n",
    "        Array array2(2, 3ul);\n",
    "        \n",
    "        {\n",
    "            Array array3(3, 5ul);\n",
    "        }\n",
    "        \n",
    "        Array array4(4, 2ul);        \n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's important to notice the ordering here: as soon as an object becomes out of scope, it is immediately destroyed; the creation order doesn't matter at all!\n",
    "\n",
    "We will see a bit [later](/notebooks/5-UsefulConceptsAndSTL/2-RAII.ipynb) how to take advantage of this behaviour to write programs that do not leak memory.\n",
    "\n",
    "\n",
    "### Default destructor\n",
    "\n",
    "If not specified, C++ implicitly defines a destructor with an empty body. Personally I like even in this case to make it explicit, which is done the same way as for a constructor from C++11 onward:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct MyClass\n",
    "{\n",
    "    \n",
    "    MyClass() = default; // explicit default constructor\n",
    "    \n",
    "    ~MyClass() = default; // explicit default destructor\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is however a matter of personal taste; see for instance [this post from FluentCpp](https://www.fluentcpp.com/2019/04/23/the-rule-of-zero-zero-constructor-zero-calorie) for the opposite advice of not defining explicitly default constructor / destructor if you don't have to."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# References\n",
    "\n",
    "[<a id=\"cit-Meyers2001\" href=\"#call-Meyers2001\">Meyers2001</a>] Scott Meyers, ``_Effective STL: 50 Specific Ways to Improve Your Use of the Standard Template Library_'',  2001.\n",
    "\n",
    "[<a id=\"cit-Meyers2015\" href=\"#call-Meyers2015\">Meyers2015</a>] Scott Meyers, ``_Effective modern C++: 42 specific ways to improve your use of C++11\n",
    "               and C++14_'',  2015.  [online](http://www.worldcat.org/oclc/890021237)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[© Copyright](../COPYRIGHT.md)   \n",
    "",
    ""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "key",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
