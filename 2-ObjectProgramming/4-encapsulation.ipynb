{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](./) - [Object programming](./0-main.ipynb) - [Encapsulation](./4-encapsulation.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Public-and-private\" data-toc-modified-id=\"Public-and-private-1\">Public and private</a></span></li><li><span><a href=\"#Struct-and-class\" data-toc-modified-id=\"Struct-and-class-2\">Struct and class</a></span></li><li><span><a href=\"#Why-encapsulation?\" data-toc-modified-id=\"Why-encapsulation?-3\">Why encapsulation?</a></span></li><li><span><a href=\"#Good-practice:-data-attributes-should-be-private\" data-toc-modified-id=\"Good-practice:-data-attributes-should-be-private-4\">Good practice: data attributes should be private</a></span></li><li><span><a href=\"#Good-practices:-the-default-status-of-a-method-should-be-private-and-you-should-use-mutators-for-quantities-that-might-vary\" data-toc-modified-id=\"Good-practices:-the-default-status-of-a-method-should-be-private-and-you-should-use-mutators-for-quantities-that-might-vary-5\">Good practices: the default status of a method should be private and you should use mutators for quantities that might vary</a></span></li><li><span><a href=\"#Friendship\" data-toc-modified-id=\"Friendship-6\">Friendship</a></span></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Public and private\n",
    "\n",
    "We've now played quite a bit with objects now, but we are still using `struct` and not `class` keyword. So it's time to try to define a class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class FirstClass\n",
    "{\n",
    "    FirstClass();\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "FirstClass::FirstClass()\n",
    "{\n",
    "    std::cout << \"Hello world!\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    FirstClass object; // COMPILATION ERROR!\n",
    "}       "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The compiler informs you that a **private** constructor was called.\n",
    "\n",
    "In fact, in C++ you we may define the level of access of attributes with three levels (we'll let the third slip until the [chapter about inheritance](./6-inheritance.ipynb#Protected-status)):\n",
    "\n",
    "* **private** means only the member functions of the class are entitled to access it (and [friends](./4-encapsulation.ipynb#Friendship) as we shall see later in this notebook)\n",
    "* **public** means everyone may access it.\n",
    "\n",
    "To determine which one is used, `public:` and `private:` may be added in a class declaration:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class SecondClass\n",
    "{\n",
    "    public:\n",
    "    \n",
    "        SecondClass(int a);\n",
    "    \n",
    "    private:\n",
    "    \n",
    "        int a_ { -9999999 }; // stupid default value.\n",
    "    \n",
    "        void SetValue(int a);\n",
    "    \n",
    "    public:\n",
    "    \n",
    "        void Print() const;    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "SecondClass::SecondClass(int a)\n",
    "{\n",
    "    SetValue(a); // Ok: a member function (here the constructor) may call private method\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void SecondClass::Print() const\n",
    "{\n",
    "    std::cout << \"The value is \" << a_ << std::endl; // Ok: a member function may use private data attribute.\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "void SecondClass::SetValue(int a)\n",
    "{\n",
    "    a_ = a; // Ok: a member function may use private data attribute.\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    SecondClass object(5);\n",
    "    object.Print(); // Ok: public method\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    SecondClass object(5);\n",
    "    object.SetValue(7); // COMPILATION ERROR: trying to call publicly a private method\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you may see on our example, there might be as many public and private sections as you wish, and their ordering doesn't matter (often coding standards recommend such an ordering, saying for instance to put public interface first, but the language itself does not care in the least).\n",
    "\n",
    "One side note for those accustomed to other languages: C++ is really hell bent about privacy status. It is not a gentleman's agreement as in Python where the `_` prefix is an indication an attribute should not be used publicly but a user may supersede the choice anyway; in C++ you can't call directly a private method of a class\n",
    "without modifying the class interface yourself - which is ill-advised, especially if we're talking about code from a third-party library.\n",
    "\n",
    "\n",
    "## Struct and class\n",
    "\n",
    "The difference in a (C++) `struct` and a `class` is in fact thin-veiled:\n",
    "\n",
    "* A `struct` assumes implicitly attributes are public if nothing is specified.\n",
    "* A `class` assumes implicitly attributes are private if nothing is specified.\n",
    "\n",
    "I would advise personally to use classes and specify explicitly the sections (as we shall see very soon it is advised to get at least some private parts) but you now understand why we stick with `struct` in the former chapters: it allowed not to meddle with public/private concerns.\n",
    "\n",
    "\n",
    "## Why encapsulation?\n",
    "\n",
    "So far we've described the mechanism, but not provided much insight on why such a hurdle was introduced in the first place. Let's see a concrete example in which encapsulation benefits appear clearly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Rectangle\n",
    "{\n",
    "    Rectangle(double length, double width);\n",
    "\n",
    "    double length_;\n",
    "    double width_;\n",
    "    double area_;\n",
    "    \n",
    "    void Print() const;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Rectangle::Rectangle(double length, double width)\n",
    ": length_(length),\n",
    "width_(width),\n",
    "area_(length * width)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void Rectangle::Print() const\n",
    "{\n",
    "    std::cout << \"My rectangle is \" << length_ << \" x \" << width_ << \" so its area is \" << area_ << std::endl;    \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Rectangle rect(5., 4.);    \n",
    "    rect.Print(); // OK    \n",
    "    rect.length_ = 23.;    \n",
    "    rect.Print(); // Not so much...    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Encapsulation may protect from that:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class MoreSecureRectangle\n",
    "{\n",
    "    public:\n",
    "    \n",
    "        MoreSecureRectangle(double length, double width);\n",
    "        void Print() const;\n",
    "\n",
    "    private:\n",
    "        \n",
    "        double length_;\n",
    "        double width_;\n",
    "        double area_;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "MoreSecureRectangle::MoreSecureRectangle(double length, double width)\n",
    ": length_(length),\n",
    "width_(width),\n",
    "area_(length * width)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void MoreSecureRectangle::Print() const\n",
    "{\n",
    "    std::cout << \"My rectangle is \" << length_ << \" x \" << width_ << \" so its area is \" << area_ << std::endl;    \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    MoreSecureRectangle rect(5., 4.);    \n",
    "    rect.Print(); // OK    \n",
    "    rect.length_ = 0.; // can't do that!   \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, we have lost functionality here... If we want to add the functionality to change the values, we need more member functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Rectangle3\n",
    "{\n",
    "     public:\n",
    "    \n",
    "        Rectangle3(double length, double widgth);\n",
    "        void Print() const;\n",
    "    \n",
    "        void SetLength(double x);\n",
    "        void SetWidth(double x);\n",
    "\n",
    "    private:\n",
    "        \n",
    "        double length_;\n",
    "        double width_;\n",
    "        double area_;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Rectangle3::Rectangle3(double length, double width)\n",
    ": length_(length),\n",
    "width_(width),\n",
    "area_(length * width)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void Rectangle3::Print() const\n",
    "{\n",
    "    std::cout << \"My rectangle is \" << length_ << \" x \" << width_ << \" so its area is \" << area_ << std::endl;    \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "void Rectangle3::SetLength(double x)\n",
    "{\n",
    "    length_ = x;\n",
    "    area_ = length_ * width_;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "void Rectangle3::SetWidth(double x)\n",
    "{\n",
    "    width_ = x;\n",
    "    area_ = length_ * width_;    \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Rectangle3 rect(5., 4.);    \n",
    "    rect.Print(); // OK    \n",
    "    rect.SetLength(23.);    \n",
    "    rect.Print(); // OK    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It should be noticed the class above is safer, but not very good nonetheless:\n",
    "\n",
    "* The computation of the area is written in three different places: constructor, `SetLength()` and `SetWidth()`... It would be better to get a private `ComputeArea()` method that does this task (granted here for this example it might be overkill, but in real cases the operation might be much more than a mere multiplication...)\n",
    "* The idea to store the `area_` is not that right here: a public method `GetArea()` or `ComputeArea()` or whatever you want to call it would be preferable here. Of course in a more complex problem it might not: it depends whether the computation is costly or not (here: not) and how often you need to use the value in average before recomputing it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Good practice: data attributes should be private\n",
    "\n",
    "In the example above, we saw that the publicly data attribute could lead to an inconsistent state within the object.\n",
    "\n",
    "It is often advised (see for instance item 22 of \\cite{Meyers2005}) to make all data attributes private, with dedicated methods to access and eventually modify those data. This approach enables fine-tuning of access: you may define whether a given **accessor** or **mutator** should be public or private."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Rectangle4\n",
    "{\n",
    "     public:\n",
    "    \n",
    "        Rectangle4(double length, double width);\n",
    "    \n",
    "        // Mutators\n",
    "        void SetLength(double x);\n",
    "        void SetWidth(double x);\n",
    "    \n",
    "        // Accessors\n",
    "        double GetLength() const;\n",
    "        double GetWidth() const;\n",
    "\n",
    "    private:\n",
    "        \n",
    "        double length_ { -1.e20 }; // a stupid value which at least is deterministically known...\n",
    "        double width_ { -1.e20 };\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Rectangle4:: Rectangle4(double length, double width)\n",
    ": length_(length),\n",
    "width_(width)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "void Rectangle4::SetLength(double x)\n",
    "{\n",
    "    length_ = x;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "void Rectangle4::SetWidth(double x)\n",
    "{\n",
    "    width_ = x;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "double Rectangle4::GetLength() const\n",
    "{\n",
    "    return length_;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "double Rectangle4::GetWidth() const\n",
    "{\n",
    "    return width_;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "double ComputeArea(const Rectangle4& r) // free function\n",
    "{\n",
    "    return r.GetLength() * r.GetWidth(); // ok: public methods! And no risk to inadvertably change the values here.\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Rectangle4 rect(4., 6.5);\n",
    "    std::cout << \"Area = \" << ComputeArea(rect) << std::endl;\n",
    "    \n",
    "    rect.SetWidth(5.);\n",
    "    std::cout << \"Area = \" << ComputeArea(rect) << std::endl;    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Good practice: the default status of a method should be private and you should use mutators for quantities that might vary\n",
    "\n",
    "In our `Rectangle4` example, all mutators and accessors were public. It is clearly what is intended here so it's fine, but as a rule you should really put in the public area what is intended to be usable by an end-user of your class. Let's spin another variation of our `Rectangle` class to illustrate this:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Rectangle5\n",
    "{\n",
    "     public:\n",
    "    \n",
    "        Rectangle5(double length, double width);\n",
    "    \n",
    "        // Mutators\n",
    "        void SetLength(double x);\n",
    "        void SetWidth(double x);\n",
    "    \n",
    "        // Accessor\n",
    "        double GetArea() const; // others accessors are not defined to limit steps to define the class\n",
    "    \n",
    "    private:\n",
    "    \n",
    "        // Mutator for the area\n",
    "        // Prototype is not bright (parameters are clearly intended to be the data attributes\n",
    "        // and a prototype with no parameters would be much wiser!)\n",
    "        // but is useful to make my point below.\n",
    "        void SetArea(double length, double width);\n",
    "    \n",
    "    private:\n",
    "        \n",
    "        double area_ { -1.e20 }; // a stupid value which at least is deterministic...\n",
    "        double length_ { -1.20 }; \n",
    "        double width_ { -1.20 };\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "void Rectangle5::SetArea(double length, double width)\n",
    "{\n",
    "    area_ = width * length;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Rectangle5:: Rectangle5(double length, double width)\n",
    ": length_(length),\n",
    "width_(width)\n",
    "{\n",
    "    SetArea(width_, length_);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "double Rectangle5::GetArea() const\n",
    "{\n",
    "    return area_;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "void Rectangle5::SetLength(double x)\n",
    "{\n",
    "    length_ = x;\n",
    "    SetArea(width_, length_);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "void Rectangle5::SetWidth(double x)\n",
    "{\n",
    "    width_ = x;\n",
    "    SetArea(width_, length_);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void Print5(const Rectangle5& r)\n",
    "{\n",
    "    std::cout << \"Area is \" << r.GetArea() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Rectangle5 rect(3., 4.);\n",
    "    Print5(rect);\n",
    "    \n",
    "    rect.SetLength(8.);\n",
    "    Print5(rect);\n",
    "    \n",
    "    rect.SetWidth(2.);\n",
    "    Print5(rect);    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It's clear that `SetArea()` has no business being called publicly: this member function is introduced here to update the area each time a dimension has changed, but it is assumed to be called with very specific arguments (the data attributes).\n",
    "\n",
    "A end-user of the class doesn't in fact even need to know there is such a method: for his purpose being able to change one dimension and to get the correct area is all that matters for him.\n",
    "\n",
    "This rather dumb example illustrates the interest of a mutator: when `SetWidth()` or `SetLength()` are called, the value is assigned **and** another operation is also performed. If when extending a class you need another operations you didn't think of in the first place (imagine for instance you need for some reason to know how many times the length was modified) you have just to modify the code in one place: the definition of the mutator (plus defining a new data attribute to store this quantity and initializing it properly). If you didn't use a mutator, you would end-up search the code for all the locations the data attributes is modified and pray you didn't miss one..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Friendship\n",
    "\n",
    "Sometimes, you may have a need to open access to the private part of a class for a very specific other class or function. You may in this case use the keyword **friend** in the class declaration:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <limits>\n",
    "\n",
    "class Vector\n",
    "{\n",
    "    public:\n",
    "    \n",
    "        Vector(double x, double y, double z);\n",
    "    \n",
    "        friend double Norm(const Vector&);\n",
    "    \n",
    "        friend class PrintVector; // In C++11 `class` became optional here.\n",
    "    \n",
    "    private:\n",
    "    \n",
    "        double x_ = std::numeric_limits<double>::min();\n",
    "        double y_ = std::numeric_limits<double>::min();    \n",
    "        double z_ = std::numeric_limits<double>::min();    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vector::Vector(double x, double y, double z)\n",
    ": x_(x),\n",
    "y_(y),\n",
    "z_(z)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <cmath>\n",
    "\n",
    "double Norm(const Vector& v)\n",
    "{\n",
    "    return std::sqrt(v.x_ * v.x_ + v.y_ * v.y_ + v.z_ * v.z_); // OK!\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class PrintVector // not the cleverest class we could write...\n",
    "{\n",
    "    public:\n",
    "    \n",
    "        PrintVector(const Vector& v);\n",
    "    \n",
    "        void Print() const;\n",
    "    \n",
    "    private:\n",
    "    \n",
    "        const Vector& vector_;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "PrintVector::PrintVector(const Vector& v)\n",
    ": vector_(v)\n",
    "{}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void PrintVector::Print() const\n",
    "{\n",
    "    std::cout << \"Content of the vector is (\" << vector_.x_ << \", \" << vector_.y_ \n",
    "        << \", \" << vector_.z_ << \").\" << std::endl; // OK because of friendship!\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Vector v(2., 3., 5.);\n",
    "\n",
    "PrintVector printer(v);\n",
    "printer.Print();\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Obviously, friendship should be used with parsimony... But it's not that much of a deal-breaker as it may seem:\n",
    "\n",
    "* The friendship must be defined in the class declaration. It means you can't use it to bypass a class encapsulation without modifying this code directly.\n",
    "* The friendship is granted to a very specific function or class, and this class must be known when the class is defined. So an ill-intentioned user can't use the function prototype to sneak into your class private parts (in fact to be completely honest we will see an exception to this statement later with [forward declaration](../6-InRealEnvironment/2-FileStructure.ipynb#Forward-declaration))."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# References\n",
    "\n",
    "(<a id=\"cit-Meyers2005\" href=\"#call-Meyers2005\">Meyers, 2005</a>) Scott Meyers, ``_Effective C++: 55 Specific Ways to Improve Your Programs and Designs (3rd Edition)_'',  2005.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[© Copyright](../COPYRIGHT.md)   \n",
    "",
    ""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
