{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](./) - [Switch statement](./Switch.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Introduction\" data-toc-modified-id=\"Introduction-1\">Introduction</a></span></li><li><span><a href=\"#break-statement\" data-toc-modified-id=\"break-statement-2\"><code>break</code> statement</a></span></li><li><span><a href=\"#default-statement\" data-toc-modified-id=\"default-statement-3\"><code>default</code> statement</a></span></li><li><span><a href=\"#Declaration-in-case\" data-toc-modified-id=\"Declaration-in-case-4\">Declaration in case</a></span></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "As mentioned in the [notebook](../1-ProceduralProgramming/2-Conditions-and-loops.ipynb) about conditions, `switch` statement is an alternative to `if..else if..else`.\n",
    "\n",
    "It is however rather limited, especially considered compared to more recent languages such as [Swift](https://developer.apple.com/swift/):\n",
    "\n",
    "* The variable is an integer, an enum or might be convertible into one of those.\n",
    "* The relationship considered is an equality."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "enum class Color { blue, red, green };"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Color my_color = Color::blue;\n",
    "    switch(my_color)\n",
    "    {\n",
    "        case Color::blue:\n",
    "            std::cout << \"Blue!\" << std::endl;\n",
    "            break;\n",
    "        case Color::red:\n",
    "            std::cout << \"Red!\" << std::endl;\n",
    "            break;\n",
    "        case Color::green:\n",
    "            std::cout << \"Green!\" << std::endl;\n",
    "            break;\n",
    "            \n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `break` statement\n",
    "\n",
    "`case` describes one possible case, and ends with the `break` statement. If `break` is not present, the next case is also considered:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Color my_color = Color::blue;\n",
    "    switch(my_color)\n",
    "    {\n",
    "        case Color::blue:\n",
    "            std::cout << \"Blue!\" << std::endl;\n",
    "        case Color::red:\n",
    "            std::cout << \"Red!\" << std::endl;\n",
    "        case Color::green:\n",
    "            std::cout << \"Green!\" << std::endl;\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, unlike the rather primitive cling here most compilers will tell you if you forget about the break (to the point C++ 17 introduced `[[fallthrough]];` to silence the warning when you really want to enable several cases until a break is found)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Color my_color = Color::blue;\n",
    "    switch(my_color)\n",
    "    {\n",
    "        case Color::blue:\n",
    "            std::cout << \"Blue!\" << std::endl;\n",
    "            [[fallthrough]]; // will silence compiler warning\n",
    "        case Color::red:\n",
    "            std::cout << \"Red!\" << std::endl;\n",
    "            [[fallthrough]]; // will silence compiler warning\n",
    "        case Color::green:\n",
    "            std::cout << \"Green!\" << std::endl;       \n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `default` statement\n",
    "\n",
    "You may add a catch-all case that will handle the cases not considered previously; just use `default` after all cases:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Color my_color = Color::green;\n",
    "    switch(my_color)\n",
    "    {\n",
    "        case Color::blue:\n",
    "            std::cout << \"Blue!\" << std::endl;\n",
    "            break;\n",
    "        case Color::red:\n",
    "            std::cout << \"Red!\" << std::endl;\n",
    "            break;\n",
    "        default:\n",
    "            std::cout << \"Neither blue nor red!\" << std::endl;\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I do not advise using it: it might hide the fact one case wasn't properly considered which is to my mind the main reason to use a `switch`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Color my_color = Color::green;\n",
    "    switch(my_color)\n",
    "    {\n",
    "        case Color::blue:\n",
    "            std::cout << \"Blue!\" << std::endl;\n",
    "            break;\n",
    "        case Color::red:\n",
    "            std::cout << \"Red!\" << std::endl;\n",
    "            break;\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Declaration in case\n",
    "\n",
    "If you need to declare a variable in a case, you need to use a block:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Color my_color = Color::green;\n",
    "    switch(my_color)\n",
    "    {\n",
    "        case Color::blue:\n",
    "            std::cout << \"Blue!\" << std::endl;\n",
    "            int i = 5; // COMPILATION ERROR!\n",
    "            break;\n",
    "        default:\n",
    "            std::cout << \"Not blue!\" << std::endl;\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Color my_color = Color::green;\n",
    "    switch(my_color)\n",
    "    {\n",
    "        case Color::blue:\n",
    "        { // Just adding these brackets will solve the issue...\n",
    "            std::cout << \"Blue!\" << std::endl;\n",
    "            int i = 5;\n",
    "            break;\n",
    "        }\n",
    "        default:\n",
    "            std::cout << \"Not blue!\" << std::endl;\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "[© Copyright](../COPYRIGHT.md)   \n",
    "",
    ""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Table of contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
