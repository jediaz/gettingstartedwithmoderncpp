{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](./) - [Home-made exception class](./HomemadeException.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Why-an-home-made-exception-class?\" data-toc-modified-id=\"Why-an-home-made-exception-class?-1\">Why an home-made exception class?</a></span><ul class=\"toc-item\"><li><span><a href=\"#Clunky-constructors-provided-in-std::exception\" data-toc-modified-id=\"Clunky-constructors-provided-in-std::exception-1.1\">Clunky constructors provided in <code>std::exception</code></a></span></li><li><span><a href=\"#std::string-for-storage\" data-toc-modified-id=\"std::string-for-storage-1.2\"><code>std::string</code> for storage</a></span></li><li><span><a href=\"#Indicating-the-file-and-line\" data-toc-modified-id=\"Indicating-the-file-and-line-1.3\">Indicating the file and line</a></span></li></ul></li><li><span><a href=\"#Implementation-of-my-home-made-class\" data-toc-modified-id=\"Implementation-of-my-home-made-class-2\">Implementation of my home-made class</a></span><ul class=\"toc-item\"><li><span><a href=\"#Exception.hpp\" data-toc-modified-id=\"Exception.hpp-2.1\">Exception.hpp</a></span></li><li><span><a href=\"#Exception.cpp\" data-toc-modified-id=\"Exception.cpp-2.2\">Exception.cpp</a></span></li><li><span><a href=\"#Call-to-use-the-exception-class\" data-toc-modified-id=\"Call-to-use-the-exception-class-2.3\">Call to use the exception class</a></span></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Why an home-made exception class?\n",
    "\n",
    "### Clunky constructors provided in `std::exception`\n",
    "\n",
    "`std::exception` provides two constructors: a copy one and a default one without arguments. The idea is that you should provide your own derived exception class for each exception in your program, for which you would override properly the `what()` virtual method to display the adequate message.\n",
    "\n",
    "I prefer to provide a simple way to provide on spot exception with a mere constructor, to be able to write something like:\n",
    "\n",
    "````\n",
    "    double x = -3.;\n",
    "    \n",
    "    if (x < -2. || x > 5.)\n",
    "        throw HomemadeException(\"x should be in [-2., 5.]\");\n",
    "````\n",
    "\n",
    "rather than much more verbosy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <exception>\n",
    "\n",
    "class RangeMinus2PlusFiveException : public std::exception\n",
    "{\n",
    "public:\n",
    "    RangeMinus2PlusFiveException() = default;\n",
    "    \n",
    "    virtual const char* what() const noexcept override\n",
    "    {\n",
    "        return \"x should be in [-2., 5.]\";\n",
    "    } \n",
    "\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    double x = -3.;\n",
    "    \n",
    "    if (x < -2. || x > 5.)\n",
    "        throw RangeMinus2PlusFiveException();\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `std::string` for storage\n",
    "\n",
    "I'm not comfortable either with the `char*` type for the `what` message, especially if you want to tailor it to provide as many information as possible to the end-user (here for instance providing the value of `x` that triggered the exception is much better).\n",
    "\n",
    "With `char*`, you might encounter rather easily two opposites issues:\n",
    "\n",
    "- The memory could leak if not properly deallocated.\n",
    "- On the contrary, if it was somehow released and the exception `what()` is asked later along the line you might end-up with garbage characters in part of your string, as the memory freed might have been used for something else.\n",
    "\n",
    "We already saw the patch to avoid this kind of issues: using a container with proper RAII ensured. `std::string`is the most obvious choice for a text message.\n",
    "\n",
    "So in my code I derive my exceptions from a homemade class which is itself derived from `std::exception`; the main difference is that the storage is done with a `std::string` and the override of the `what()` method reads this `std::string`.\n",
    "\n",
    "### Indicating the file and line\n",
    "\n",
    "If your code is huge, knowing the exception itself is not enough, if this exception may be thrown from several locations in your code. To identify where the exception was thrown, I use the `__FILE__` and `__LINE__` macro which gives the file and line where they were found in the code (better alternative `std::source_location` is present in C++ 20 but is unfortunately [not well supported yet](https://en.cppreference.com/w/cpp/compiler_support)).\n",
    "\n",
    "So my constructor looks like:\n",
    "\n",
    "````\n",
    "HomemadeException(const std::string& msg, const char* invoking_file, int invoking_line);\n",
    "````\n",
    "\n",
    "It might be a bit wordy but:\n",
    "* It's safe.\n",
    "* Print is correct.\n",
    "* It is easy to figure out where it happened.\n",
    "\n",
    "## Implementation of my home-made class\n",
    "\n",
    "Here is a transcript of my own exception class, which may be found in [my main project](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/tree/master/Sources/Utilities/Exceptions). Namespace has ben removed (you should add one if you intend to use it in practice):\n",
    "\n",
    "### Exception.hpp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <string>\n",
    "#include <exception>\n",
    "\n",
    "/*!\n",
    " * \\brief Generic class for MoReFEM exceptions.\n",
    " */\n",
    "class Exception: public std::exception\n",
    "{\n",
    "public:\n",
    "\n",
    "    /// \\name Special members.\n",
    "    ///@{\n",
    "\n",
    "    /*!\n",
    "     * \\brief Constructor with simple message.\n",
    "     *\n",
    "     * \\param[in] msg Message.\n",
    "     * \\param[in] invoking_file File that invoked the function or class; usually __FILE__.\n",
    "     * \\param[in] invoking_line File that invoked the function or class; usually __LINE__.\n",
    "     */\n",
    "\n",
    "    //@}\n",
    "    explicit Exception(const std::string& msg, const char* invoking_file, int invoking_line);\n",
    "\n",
    "    //! Destructor\n",
    "    virtual ~Exception() noexcept override;\n",
    "\n",
    "    //! Copy constructor.\n",
    "    Exception(const Exception&) = default;\n",
    "\n",
    "    //! Move constructor.\n",
    "    Exception(Exception&&) = default;\n",
    "\n",
    "    //! Copy affectation.\n",
    "    Exception& operator=(const Exception&) = default;\n",
    "\n",
    "    //! Move affectation.\n",
    "    Exception& operator=(Exception&&) = default;\n",
    "\n",
    "    ///@}\n",
    "\n",
    "    //! Display the what message from std::exception.\n",
    "    virtual const char* what() const noexcept override final;\n",
    "\n",
    "    /*!\n",
    "     * \\brief Display the raw message (Without file and line).\n",
    "     *\n",
    "     * Might be useful if exception is caught to rewrite a more refined message.\n",
    "     *\n",
    "     * Before introducing this, we could end up with something like:\n",
    "     * \\verbatim\n",
    "     * Exception caught: Exception found at Sources/Model/Internal/InitializeHelper.hxx, line 114: Ill-defined\n",
    "     * finite element space 1: Exception found at Sources/Model/Internal/InitializeHelper.hxx, line 101:\n",
    "     * Domain 1 is not defined!\n",
    "     * \\endverbatim\n",
    "     *\n",
    "     * Clearly it is nicer to provide:\n",
    "     * \\verbatim\n",
    "     * Exception caught: Exception found at Sources/Model/Internal/InitializeHelper.hxx, line 114: Ill-defined\n",
    "     * finite element space 1: Domain 1 is not defined!\n",
    "     * \\endverbatim\n",
    "     *\n",
    "     * \\return Exception error message without information about file and line in which the exception was invoked.\n",
    "     */\n",
    "    const std::string& GetRawMessage() const noexcept;\n",
    "\n",
    "\n",
    "private:\n",
    "\n",
    "    //! The complete what() message (with the location part)\n",
    "    std::string what_message_;\n",
    "\n",
    "    //! Incomplete message (might be useful if we catch an exception to tailor a more specific message).\n",
    "    std::string raw_message_;\n",
    "\n",
    "};\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exception.cpp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "#include <sstream>\n",
    "#include <utility>\n",
    "\n",
    "// #include \"Exception.hpp\"\n",
    "\n",
    "\n",
    "namespace // anonymous\n",
    "{\n",
    "    \n",
    "    \n",
    "    //! Call this function to set the message displayed by the exception.\n",
    "    void SetWhatMessage(const std::string& msg, std::string& what_message, const char* invoking_file, int invoking_line)\n",
    "    {\n",
    "        std::ostringstream stream;\n",
    "        stream << \"Exception found at \";\n",
    "        stream << invoking_file << \", line \" << invoking_line << \": \";\n",
    "        stream << msg;\n",
    "        what_message = stream.str();\n",
    "    }\n",
    "    \n",
    "    \n",
    "} // namespace anonymous\n",
    "\n",
    "\n",
    "Exception::Exception(const std::string& msg, const char* invoking_file, int invoking_line)\n",
    ": std::exception(),\n",
    "raw_message_(msg)\n",
    "{\n",
    "    SetWhatMessage(msg, what_message_, invoking_file, invoking_line);\n",
    "}\n",
    "\n",
    "\n",
    "Exception::~Exception() noexcept = default;\n",
    "\n",
    "\n",
    "const char* Exception::what() const noexcept\n",
    "{\n",
    "    return what_message_.c_str();\n",
    "}\n",
    "\n",
    "\n",
    "const std::string& Exception::GetRawMessage() const noexcept\n",
    "{\n",
    "    return raw_message_;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Call to use the exception class"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    throw Exception(\"Example of exception with home-made class\", __FILE__, __LINE__);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    int i = -5;\n",
    "    std::ostringstream oconv;\n",
    "    oconv << i << \" should have been even!\";\n",
    "    \n",
    "    throw Exception(oconv.str(), __FILE__, __LINE__);\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "[© Copyright](../COPYRIGHT.md)   \n",
    "",
    ""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Table of contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
