{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](./) - [Curiously recurrent template pattern](./Crtp.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Introduction\" data-toc-modified-id=\"Introduction-1\">Introduction</a></span></li><li><span><a href=\"#Attempt-without-CRTP\" data-toc-modified-id=\"Attempt-without-CRTP-2\">Attempt without CRTP</a></span><ul class=\"toc-item\"><li><span><a href=\"#First-attempt:-public-inheritance\" data-toc-modified-id=\"First-attempt:-public-inheritance-2.1\">First attempt: public inheritance</a></span></li><li><span><a href=\"#Second-attempt:-private-inheritance\" data-toc-modified-id=\"Second-attempt:-private-inheritance-2.2\">Second attempt: private inheritance</a></span></li><li><span><a href=\"#Third-attempt:-composition\" data-toc-modified-id=\"Third-attempt:-composition-2.3\">Third attempt: composition</a></span></li></ul></li><li><span><a href=\"#CRTP\" data-toc-modified-id=\"CRTP-3\">CRTP</a></span><ul class=\"toc-item\"><li><span><a href=\"#Referring-to-the-base-class\" data-toc-modified-id=\"Referring-to-the-base-class-3.1\">Referring to the base class</a></span></li><li><span><a href=\"#Never-call-a-CRTP-method-in-base-constructor!\" data-toc-modified-id=\"Never-call-a-CRTP-method-in-base-constructor!-3.2\">Never call a CRTP method in base constructor!</a></span></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "The **curiously recurrent template pattern** (often shortened as **CRTP**) is a idiom that might put you off guard the first time you meet it: it's not exactly obvious it should be allowed to compile...\n",
    "\n",
    "The syntax is:\n",
    "\n",
    "````\n",
    "class Derived : public Base<Derived>\n",
    "````\n",
    "\n",
    "i.e. the class derives from a template specialization of a base class... which template argument is the class itself!\n",
    "\n",
    "The purpose of this idiom is to provide a specific behaviour to several classes that might be unrelated otherwise.\n",
    "\n",
    "Throughout this tutorial, we will consider the functionality of giving to an object a unique identifier: each time a new object is created, we want to provide to it a unique index and the accessor to get it. We'll try first what we already have in store and see why the CRTP is very handy to get.\n",
    "\n",
    "Of course, some of the limitations we shall see stem from the use of a static method; however this is an example among others of cases that aren't properly supported by the basic inheritance or composition.\n",
    "\n",
    "\n",
    "## Attempt without CRTP\n",
    "\n",
    "### First attempt: public inheritance\n",
    "\n",
    "The first idea should be for most of you to use inheritance, probably in its public form.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "using uint = unsigned int; // just to bypass a Xeus-cling limitation..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class UniqueId\n",
    "{\n",
    "public:\n",
    "    UniqueId();\n",
    "    \n",
    "    // Allowing copy would defeat the purpose of such a class...\n",
    "    UniqueId(const UniqueId&) = delete;\n",
    "    UniqueId& operator=(const UniqueId&) = delete;\n",
    "    \n",
    "    uint GetUniqueId() const; // noexcept would be better, but cling disagree...\n",
    "    \n",
    "private:   \n",
    "    \n",
    "    const uint unique_id_;\n",
    "    \n",
    "    static uint Generate();\n",
    "\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uint UniqueId::Generate()\n",
    "{\n",
    "    static auto ret = 0u;\n",
    "    return ret++;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "UniqueId::UniqueId()\n",
    ": unique_id_(Generate())\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uint UniqueId::GetUniqueId() const\n",
    "{\n",
    "    return unique_id_;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Carrot : public UniqueId\n",
    "{ };"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Cabbage : public UniqueId\n",
    "{ };"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Carrot carrot[2];\n",
    "    Cabbage cabbage[2];    \n",
    "    \n",
    "    for (auto i = 0ul; i < 2; ++i)\n",
    "    {\n",
    "        std::cout << \"Carrot \" << carrot[i].GetUniqueId() << std::endl;\n",
    "        std::cout << \"Cabbage \" << cabbage[i].GetUniqueId() << std::endl;        \n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "That is not exactly what we had in mind: the idea of providing the same functionality for `Carrot` and `Cabbage` is appealing, but it would have been better not to share the same index (and we can imagine more complex CRTP for which we absolutely do not want to intertwine the derived classes).\n",
    "\n",
    "Besides that, there is another drawback: doing so enables a mischiever developer to store in a same container otherwise unrelated classes as pointers:\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "\n",
    "{\n",
    "    Carrot* carrot = new Carrot;\n",
    "    Cabbage* cabbage = new Cabbage;\n",
    "    \n",
    "    std::vector<UniqueId*> list { carrot, cabbage };\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, at some point you can't protect a developer against his/her own stupidity, but ideally it's better if you can protect it as much a possible, and closing this possibility would be nice.\n",
    "\n",
    "In case you're wondering, the list above would not be terribly useful: if you want to use the list for something other that the unique id functionality, you would have to use **dynamic_cast** - which I do not recommend) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Second attempt: private inheritance"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Carrot2 : private UniqueId\n",
    "{ \n",
    "    using UniqueId::GetUniqueId;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Cabbage2 : private UniqueId\n",
    "{ \n",
    "    using UniqueId::GetUniqueId;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Carrot2 carrot[2];\n",
    "    Cabbage2 cabbage[2];    \n",
    "    \n",
    "    for (auto i = 0ul; i < 2; ++i)\n",
    "    {\n",
    "        std::cout << \"Carrot \" << carrot[i].GetUniqueId() << std::endl;\n",
    "        std::cout << \"Cabbage \" << cabbage[i].GetUniqueId() << std::endl;        \n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We haven't gained much with private inheritance, and even lost a bit:\n",
    "\n",
    "* Underlying numeration is still the same for unrelated classes.\n",
    "* We now additionally need to explicitly allow in derived classes the base class method with `using` statement. This may not seem much, but in a complex CRTP with several methods it adds boilerplate to provide in each derived class.\n",
    "* It is up to the user to proceed to private inheritance: he may use public one as easily as private one.\n",
    "\n",
    "The only substantive gain is that we block the possibility to list together unrelated objects:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "\n",
    "{\n",
    "    Carrot2* carrot = new Carrot2;\n",
    "    Cabbage2* cabbage = new Cabbage2;\n",
    "    \n",
    "    std::vector<UniqueId*> list { carrot, cabbage };\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Third attempt: composition\n",
    "\n",
    "Composition actually displays most of the same weaknesses as the private inheritance, and is also more wordy to use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <memory>\n",
    "\n",
    "class Carrot3\n",
    "{ \n",
    "    \n",
    "public:\n",
    "    \n",
    "    Carrot3();    \n",
    "    \n",
    "    uint GetUniqueId() const;\n",
    "    \n",
    "private:\n",
    "    \n",
    "    const std::unique_ptr<UniqueId> unique_id_ = nullptr;\n",
    "    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Carrot3::Carrot3()\n",
    ": unique_id_(std::make_unique<UniqueId>())\n",
    "{ }\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uint Carrot3::GetUniqueId() const\n",
    "{\n",
    "    assert(!(!unique_id_));\n",
    "    return unique_id_->GetUniqueId();\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Cabbage3\n",
    "{ \n",
    "    \n",
    "public:\n",
    "    \n",
    "    Cabbage3();    \n",
    "    \n",
    "    uint GetUniqueId() const;\n",
    "    \n",
    "private:\n",
    "    \n",
    "    const std::unique_ptr<UniqueId> unique_id_ = nullptr;\n",
    "    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Cabbage3::Cabbage3()\n",
    ": unique_id_(std::make_unique<UniqueId>())\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "uint Cabbage3::GetUniqueId() const\n",
    "{\n",
    "    assert(!(!unique_id_));\n",
    "    return unique_id_->GetUniqueId();\n",
    "}\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Carrot3 carrot[2];\n",
    "    Cabbage3 cabbage[2];    \n",
    "    \n",
    "    for (auto i = 0ul; i < 2; ++i)\n",
    "    {\n",
    "        std::cout << \"Carrot \" << carrot[i].GetUniqueId() << std::endl;\n",
    "        std::cout << \"Cabbage \" << cabbage[i].GetUniqueId() << std::endl;        \n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So it's mostly more of the same; it is pretty wordy but at least unrelated objects can't be put in the same container."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## CRTP\n",
    "\n",
    "Now let's write a CRTP:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<class DerivedT> \n",
    "class CrtpUniqueId\n",
    "{\n",
    "public:\n",
    "    CrtpUniqueId();\n",
    "    \n",
    "    // Allowing copy would defeat the purpose of such a class...\n",
    "    CrtpUniqueId(const CrtpUniqueId&) = delete;\n",
    "    CrtpUniqueId& operator=(const CrtpUniqueId&) = delete;\n",
    "    \n",
    "    uint GetUniqueId() const; // noexcept would be better, but cling disagree...\n",
    "    \n",
    "private:   \n",
    "    \n",
    "    const uint unique_id_;\n",
    "    \n",
    "    static uint Generate();\n",
    "\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<class DerivedT>\n",
    "uint CrtpUniqueId<DerivedT>::Generate()\n",
    "{\n",
    "    static auto ret = 0u;\n",
    "    return ret++;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<class DerivedT>\n",
    "CrtpUniqueId<DerivedT>::CrtpUniqueId()\n",
    ": unique_id_(Generate())\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<class DerivedT>\n",
    "uint CrtpUniqueId<DerivedT>::GetUniqueId() const\n",
    "{\n",
    "    return unique_id_;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And now define the derived class with this curious syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Carrot4 : public CrtpUniqueId<Carrot4>\n",
    "{ };"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct Cabbage4 : public CrtpUniqueId<Cabbage4>\n",
    "{ };"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Carrot4 carrot[2];\n",
    "    Cabbage4 cabbage[2];    \n",
    "    \n",
    "    for (auto i = 0ul; i < 2; ++i)\n",
    "    {\n",
    "        std::cout << \"Carrot \" << carrot[i].GetUniqueId() << std::endl;\n",
    "        std::cout << \"Cabbage \" << cabbage[i].GetUniqueId() << std::endl;        \n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see with this syntax that:\n",
    "\n",
    "* Each class properly use its own internal numbering.\n",
    "* There are no relationship at all between derived classes: `CrtpUniqueId<Carrot4>` is an entirely different object than `CrtpUniqueId<Cabbage4>`.\n",
    "* There are no boilerplate in derived classes implementation: nothing to add besides the CRTP syntax.\n",
    "\n",
    "Moreover, this occurs at compilation and there are no runtime cost whatsoever.\n",
    "\n",
    "The inheritance might be public, protected or private, depending on your needs.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Referring to the base class\n",
    "\n",
    "Sometimes however, you need to access something from the derived class in the CRTP, and I must admit in this case the syntax could have been more sweet..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<class DerivedT>\n",
    "struct CrtpPrint\n",
    "{\n",
    "    \n",
    "    void Print() const;\n",
    "    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "template<class DerivedT>\n",
    "void CrtpPrint<DerivedT>::Print() const\n",
    "{\n",
    "    std::cout << \"The name of the class is \" << static_cast<const DerivedT&>(*this).ClassName() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <string>\n",
    "\n",
    "struct Tomato : public CrtpPrint<Tomato>\n",
    "{\n",
    "    static std::string ClassName();\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "std::string Tomato::ClassName()\n",
    "{\n",
    "    return std::string(\"Tomato\");\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Tomato tomato;\n",
    "    tomato.Print();\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we saw, the way to access something in the base class is to use:\n",
    "\n",
    "````\n",
    "static_cast<DerivedT&>(*this)\n",
    "````\n",
    "\n",
    "In the case above, I added a `const` as we wanted to access a constant method of the base class.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Never call a CRTP method in base constructor!\n",
    "\n",
    "CRTP is sometimes dubbed **static polymorphism** due to this functionality. This name is helpful in the sense a warning true for usual dynamic polymorphism is also true here: **never** call a derived method in the base constructor: it would lead to undefined behaviour."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "[© Copyright](../COPYRIGHT.md)   \n",
    "",
    ""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
