© _CNRS 2016_ - _Inria 2018-2022_   

This notebook is an adaptation of a [lecture](https://gitlab.inria.fr/formations/cpp/DebuterEnCpp) prepared by David Chamont (CNRS) with the help of Vincent Rouvreau (Inria) under the terms of the licence [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](http://creativecommons.org/licenses/by-nc-sa/4.0/)

Present version has been written by several Inria engineers:

- Sébastien Gilles (SED Saclay): _2018 - present_
- Vincent Rouvreau (SED Saclay): _2018 - present_
- Vicente Mataix Ferrandiz (SED Paris): _2021_
- Laurent Steff (SED Saclay): _2021 - present_

and is using the same licence.