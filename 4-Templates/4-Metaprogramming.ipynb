{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](./) - [Templates](./0-main.ipynb) - [Metaprogramming](./4-Metaprogramming.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Introduction\" data-toc-modified-id=\"Introduction-1\">Introduction</a></span></li><li><span><a href=\"#Example:-same-action-upon-a-collection-of-heterogeneous-objects\" data-toc-modified-id=\"Example:-same-action-upon-a-collection-of-heterogeneous-objects-2\">Example: same action upon a collection of heterogeneous objects</a></span></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "We will no go very far in this direction: metaprogramming is really a very rich subset of C++ of its own, and one that is especially tricky to code.\n",
    "\n",
    "You can be a very skilled C++ developer and never use this; however it opens some really interesting prospects that can't be achieved easily (or at all...) without it.\n",
    "\n",
    "I recommend the reading of \\cite{Alexandrescu2001} to get the gist of it: even if it relies upon older versions of C++ (and therefore some of its hand-made constructs are now in one form or another in modern C++ or STL) it is very insightful to understand the reasoning behind metaprogrammation.\n",
    "\n",
    "\n",
    "## Example: same action upon a collection of heterogeneous objects\n",
    "\n",
    "Let's say we want to put together in a same container multiple objects of heterogeneous type (one concrete case for which I have used that: reading an input data file with each entry is handled differently by a dedicated object).\n",
    "\n",
    "In C++11, `std::tuple` was introduced for that purpose:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <tuple>\n",
    "   \n",
    "std::tuple<int, std::string, double, float, long> tuple = \n",
    "    std::make_tuple(5, \"hello\", 5., 3.2f, -35l);\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What if we want to apply the same treatment of all of the entries? Let's roll with just printing each of them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::cout << std::get<0>(tuple) << std::endl;\n",
    "    std::cout << std::get<1>(tuple) << std::endl;\n",
    "    std::cout << std::get<2>(tuple) << std::endl;\n",
    "    std::cout << std::get<3>(tuple) << std::endl;\n",
    "    std::cout << std::get<4>(tuple) << std::endl;\n",
    "}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The treatment was rather simple here, but the code was duplicated manually for each of them. **Metaprogramming** is the art of making the compiler generate by itself the whole code. \n",
    "\n",
    "The syntax relies heavily on templates, and those of you familiar with functional programming will feel at ease here:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "template <std::size_t IndexT, std::size_t TupleSizeT>\n",
    "struct PrintTuple\n",
    "{\n",
    "        \n",
    "    template<class TupleT> // I'm lazy I won't put there std::tuple<int, std::string, double, float, long> again... \n",
    "    static void Do(const TupleT& tuple)\n",
    "    {\n",
    "        std::cout << std::get<IndexT>(tuple) << std::endl;\n",
    "        PrintTuple<IndexT + 1ul, TupleSizeT>::Do(tuple); // that's the catch: call recursively the next one!\n",
    "    }    \n",
    "    \n",
    "};"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may see the gist of it, but there is still an issue: the recursivity goes to the infinity... (don't worry your compiler will yell before that!). So you need a specialization to stop it - you may see now why I used a class template and not a function!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template <std::size_t TupleSizeT>\n",
    "struct PrintTuple<TupleSizeT, TupleSizeT>\n",
    "{\n",
    "        \n",
    "    template<class TupleT>\n",
    "    static void Do(const TupleT& tuple)\n",
    "    {\n",
    "        // Do nothing!\n",
    "    }    \n",
    "    \n",
    "};"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With that, the code is properly generated:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    std::tuple<int, std::string, double, float, long> tuple = \n",
    "        std::make_tuple(5, \"hello\", 5., 3.2f, -35l);\n",
    "    PrintTuple<0, std::tuple_size<decltype(tuple)>::value>::Do(tuple);\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, the call is not yet very easy: it's cumbersome to have to explicitly reach the tuple size... But as often in C++ an extra level of indirection may lift this issue:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<class TupleT>\n",
    "void PrintTupleWrapper(const TupleT& t)\n",
    "{\n",
    "    PrintTuple<0, std::tuple_size<TupleT>::value>::Do(t);\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And then the call may be simply:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    std::tuple<int, std::string, double, float, long> tuple = std::make_tuple(5, \"hello\", 5., 3.2f, -35l);\n",
    "    PrintTupleWrapper(tuple);\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In fact, my laziness earlier when I used a template argument rather than the exact tuple type pays now as this function may be used with any tuple (or more precisely with any tuple for which all elements comply with `operator<<`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    int a = 5;\n",
    "    std::tuple<std::string, int*> tuple = std::make_tuple(\"Hello\", &a);\n",
    "    PrintTupleWrapper(tuple);\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bonus: metaprogramming Fibonacci\n",
    "\n",
    "In [notebook about constexpr](../1-ProceduralProgramming/7-StaticAndConstexpr.ipynb), I said implementing Fibonacci series before C++ 11 involved metaprogramming; here is an implementation (much more wordy than the `constexpr` one):\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<std::size_t N>\n",
    "struct Fibonacci\n",
    "{\n",
    " \n",
    "    static std::size_t Do()\n",
    "    {\n",
    "        return Fibonacci<N-1>::Do() + Fibonacci<N-2>::Do();\n",
    "    }\n",
    "    \n",
    "    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Don't forget the specialization for 0 and 1!\n",
    "\n",
    "template<>\n",
    "struct Fibonacci<0ul>\n",
    "{\n",
    " \n",
    "    static std::size_t Do()\n",
    "    {\n",
    "        return 0ul;\n",
    "    }\n",
    "    \n",
    "    \n",
    "};\n",
    "\n",
    "\n",
    "template<>\n",
    "struct Fibonacci<1ul>\n",
    "{\n",
    " \n",
    "    static std::size_t Do()\n",
    "    {\n",
    "        return 1ul;\n",
    "    }\n",
    "    \n",
    "    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "std::cout << Fibonacci<5ul>::Do() << std::endl;\n",
    "std::cout << Fibonacci<10ul>::Do() << std::endl;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And if the syntax doesn't suit you... you could always add an extra level of indirection to remove the `::Do()` part:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<std::size_t N>\n",
    "std::size_t FibonacciWrapper()\n",
    "{\n",
    "    return Fibonacci<N>::Do();\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "std::cout << FibonacciWrapper<5ul>() << std::endl;\n",
    "std::cout << FibonacciWrapper<10ul>() << std::endl;\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, in some cases `constexpr` really alleviates some tedious boilerplate... \n",
    "\n",
    "It should be noticed that although these computations really occur at compile time, they aren't nonetheless recognized automatically as `constexpr`: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "constexpr auto fibo_5 = FibonacciWrapper<5ul>(); // COMPILATION ERROR!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To fix that, you need to declare `constexpr`:\n",
    "- Each of the `Do` static method (`static constexpr std::size_t Do()`)\n",
    "- The `FibonacciWrapper` function (`template<std::size_t N> constexpr std::size_t FibonacciWrapper()`)\n",
    "\n",
    "So in this specific case you should really go with the much less wordy and more expressive expression with `constexpr` given in [aforementioned notebook](../1-ProceduralProgramming/7-StaticAndConstexpr.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# References\n",
    "\n",
    "[<a id=\"cit-Alexandrescu2001\" href=\"#call-Alexandrescu2001\">Alexandrescu2001</a>] Andrei Alexandrescu, ``_Modern C++ Design: Generic Programming and Design Patterns applied_'', 01 2001.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[© Copyright](../COPYRIGHT.md)\n",
    "",
    ""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": false,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "key",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "393px"
   },
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
