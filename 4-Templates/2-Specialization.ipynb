{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](./) - [Templates](./0-main.ipynb) - [Specialization](./2-Specialization.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Total-specialization\" data-toc-modified-id=\"Total-specialization-1\">Total specialization</a></span><ul class=\"toc-item\"><li><span><a href=\"#For-template-functions\" data-toc-modified-id=\"For-template-functions-1.1\">For template functions</a></span></li><li><span><a href=\"#For-template-classes\" data-toc-modified-id=\"For-template-classes-1.2\">For template classes</a></span></li></ul></li><li><span><a href=\"#Partial-specialization\" data-toc-modified-id=\"Partial-specialization-2\">Partial specialization</a></span><ul class=\"toc-item\"><li><span><a href=\"#For-classes\" data-toc-modified-id=\"For-classes-2.1\">For classes</a></span></li><li><span><a href=\"#But-not-for-functions!\" data-toc-modified-id=\"But-not-for-functions!-2.2\">But not for functions!</a></span></li><li><span><a href=\"#Mimicking-the-partial-template-specialization-for-functions\" data-toc-modified-id=\"Mimicking-the-partial-template-specialization-for-functions-2.3\">Mimicking the partial template specialization for functions</a></span><ul class=\"toc-item\"><li><span><a href=\"#Using-a-class-and-a-static-method\" data-toc-modified-id=\"Using-a-class-and-a-static-method-2.3.1\">Using a class and a static method</a></span></li></ul></li><li><span><a href=\"#If-constexpr\" data-toc-modified-id=\"If-constexpr-2.4\">If constexpr</a></span></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Total specialization\n",
    "\n",
    "### For template functions\n",
    "\n",
    "Let's take back our previous `Convert` example: the conversion into a `std::string` wasn't valid with the proposed instantiation, but converting an integer into a `std::string` is not an outlandish idea either...\n",
    "\n",
    "There is a mechanism to provide specific instantiation for a type: the **(total) specialization** (we'll see why _total_ shortly):\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "template<class T>\n",
    "T Convert(int value)\n",
    "{\n",
    "    std::cout << \"Generic instantiation called!\" << std::endl;\n",
    "    return static_cast<T>(value);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <string>\n",
    "\n",
    "template<> // notice the empty brackets!\n",
    "std::string Convert<std::string>(int value)\n",
    "{\n",
    "    std::cout << \"std::string instantiation called!\" << std::endl;    \n",
    "    return std::to_string(value);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Convert<double>(5);\n",
    "    Convert<std::string>(5);    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please notice the syntax: the definition provides template with no parameter in the brackets, and the `T` is replaced by the specialized type. As there are no template parameters left, the definition of the specialization must be put in a compiled file (or with an [`inline` keyword](../1-ProceduralProgramming/4-Functions.ipynb#inline-functions) in header file)\n",
    "\n",
    "Of course, as many specialization as you wish may be provided:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class HoldAnInt\n",
    "{\n",
    "    public:\n",
    "        explicit HoldAnInt(int value);\n",
    "    \n",
    "    private:\n",
    "        int value_;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "HoldAnInt::HoldAnInt(int value)\n",
    ": value_(value)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<>\n",
    "HoldAnInt Convert(int value)\n",
    "{\n",
    "    std::cout << \"HoldAnInt instantiation called!\" << std::endl;    \n",
    "    return HoldAnInt(value);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    Convert<double>(5);\n",
    "    Convert<std::string>(5);    \n",
    "    Convert<HoldAnInt>(5);        \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### For template classes\n",
    "\n",
    "Total specialization was illustrated on a function, but it works as well for a class. You may choose to:\n",
    "\n",
    "* Specialize either the entire class (in which case you have to define the complete API for the specialized class) \n",
    "* Or just specialize a method.\n",
    "\n",
    "The case below provides entire class specialization for `double` and just method specialization for `std::string`:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// First the generic class `HoldAValue`\n",
    "\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "\n",
    "template<class T>\n",
    "class HoldAValue\n",
    "{\n",
    "    public:\n",
    "    \n",
    "        HoldAValue(T value);\n",
    "        \n",
    "        T GetValue() const;\n",
    "        \n",
    "    private:\n",
    "    \n",
    "        T value_;    \n",
    "};\n",
    "\n",
    "\n",
    "template<class T>\n",
    "HoldAValue<T>::HoldAValue(T value)\n",
    ": value_(value)\n",
    "{ }\n",
    "\n",
    "\n",
    "template<class T>\n",
    "T HoldAValue<T>::GetValue() const\n",
    "{ \n",
    "    return value_;    \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Method specialization for std::string\n",
    "\n",
    "template<>\n",
    "std::string HoldAValue<std::string>::GetValue() const\n",
    "{ \n",
    "    return \"String case (through method specialization): \" + value_;    \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Class specialization for double\n",
    "\n",
    "template<>\n",
    "class HoldAValue<double>\n",
    "{\n",
    "    public:\n",
    "    \n",
    "        HoldAValue(std::string blah, double value);\n",
    "        \n",
    "        double GetValue() const;\n",
    "        \n",
    "    private:\n",
    "    \n",
    "        std::string blah_;\n",
    "    \n",
    "        double value_;   \n",
    "};\n",
    "\n",
    "\n",
    "HoldAValue<double>::HoldAValue(std::string blah, double value)\n",
    ": blah_(blah),\n",
    "value_(value)\n",
    "{ }\n",
    "\n",
    "\n",
    "double HoldAValue<double>::GetValue() const\n",
    "{ \n",
    "    std::cout << blah_;\n",
    "    return value_;    \n",
    "}\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    \n",
    "    HoldAValue<int> integer(5);\n",
    "    std::cout << integer.GetValue() << std::endl;\n",
    "    \n",
    "    HoldAValue<std::string> string(\"Hello world\");\n",
    "    std::cout << string.GetValue() << std::endl;\n",
    "    \n",
    "    HoldAValue<double> dbl(\"Double case (through class specialization): \", 3.14);\n",
    "    std::cout << dbl.GetValue() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Partial specialization\n",
    "\n",
    "### For classes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also possible for classes to **specialize partially** a template class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<class TypeT, std::size_t Nelts>\n",
    "class MyArray\n",
    "{\n",
    "    public:\n",
    "        \n",
    "        explicit MyArray(TypeT initial_value);\n",
    "    \n",
    "    private:\n",
    "    \n",
    "        TypeT content_[Nelts];\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "template<class TypeT, std::size_t Nelts>\n",
    "MyArray<TypeT, Nelts>::MyArray(TypeT initial_value)\n",
    "{\n",
    "    std::cout << \"Generic constructor\" << std::endl;\n",
    "    for (auto i = 0ul; i < Nelts; ++i)\n",
    "        content_[i] = initial_value;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<class TypeT>\n",
    "class MyArray<TypeT, 10ul>\n",
    "{\n",
    "    public:\n",
    "        \n",
    "        explicit MyArray(TypeT initial_value);\n",
    "    \n",
    "    private:\n",
    "    \n",
    "        TypeT content_[10ul];\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "template<class TypeT>\n",
    "MyArray<TypeT, 10ul>::MyArray(TypeT initial_value)\n",
    "{\n",
    "    std::cout << \"Partial specialization constructor: type is still templated but size is fixed \" << std::endl;\n",
    "    for (auto i = 0ul; i < 10ul; ++i)\n",
    "        content_[i] = initial_value;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    MyArray<int, 8ul> generic_array(2); \n",
    "    MyArray<int, 10ul> specific_array(2); \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### But not for functions!\n",
    "\n",
    "However, partial specialization is **forbidden** for template functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "template<class T, class U>\n",
    "void PrintSquare(T t, U u)\n",
    "{\n",
    "    std::cout << \"Generic instantiation\" << std::endl;\n",
    "    std::cout << \"(t^2, u^2) = (\" << t * t << \", \" << u * u << \")\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    PrintSquare(5, 7.);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <string>\n",
    "\n",
    "template<class T>\n",
    "void PrintSquare<T, std::string>(T t, std::string u) // COMPILATION ERROR!\n",
    "{\n",
    "    std::cout << \"Partial function specialization: doesn't compile!\" << std::endl;    \n",
    "    std::cout << \"(t^2, u^2) = (\" << t * t << \", \" << (u + u) << \")\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You might have the impress it works if you try defining the function without specifying the brackets\n",
    "\n",
    "_(please use [@Coliru](https://coliru.stacked-crooked.com/a/b1504e7033fd9346) - the code below no longer works in Xeus-cling as of September 2022)_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// DOESN'T WORK ON XEUS-CLING!\n",
    "\n",
    "#include <iostream>\n",
    "#include <string>\n",
    "\n",
    "template<class T, class U>\n",
    "void PrintSquare(T t, U u)\n",
    "{\n",
    "    std::cout << \"Generic instantiation\" << std::endl;\n",
    "    std::cout << \"(t^2, u^2) = (\" << t * t << \", \" << u * u << \")\" << std::endl;\n",
    "}\n",
    "\n",
    "\n",
    "template<class T>\n",
    "void PrintSquare(T t, std::string u)\n",
    "{\n",
    "    std::cout << \"Seemingly ok function template specialization \" << std::endl;\n",
    "    std::cout << \"(t^2, u^2) = (\" << t * t << \", \" << (u + u) << \")\" << std::endl;\n",
    "}\n",
    "\n",
    "\n",
    "int main(int argc, char** argv)\n",
    "{\n",
    "    std::string hello(\"Hello\");\n",
    "    PrintSquare(5., hello);\n",
    "    \n",
    "    // But if you uncomment this line, the compilation will fail...\n",
    "    // PrintSquare<double, std::string>(5., std::string(\"Hello\"));\n",
    "    \n",
    "    // ... whereas it is a perfectly legit rule of instantiation, as you may check with:\n",
    "    // PrintSquare<double, int>(5., 3);\n",
    "    \n",
    "    \n",
    "    return EXIT_SUCCESS;  \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To be honest, the reason partial specialization are forbidden are quite unclear for me; this seems to be internal reasons in the way compilers are parsing the C++ code. I've seen on forums discussions to lift this limitation over the years, but to my knowledge no concrete plan is in motion to do so.\n",
    "\n",
    "### Mimicking the partial template specialization for functions\n",
    "\n",
    "#### Using a class and a static method\n",
    "\n",
    "There is a quite verbosy way to circumvent the impossibility: use a template struct (which is partially specializable) with a static method..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "template<class T, class U>\n",
    "struct PrintSquareHelper\n",
    "{\n",
    "    \n",
    "    static void Do(T t, U u)\n",
    "    {\n",
    "        std::cout << \"Generic instantiation\" << std::endl;\n",
    "        std::cout << \"(t^2, u^2) = (\" << t * t << \", \" << u * u << \")\" << std::endl;\n",
    "    }\n",
    "    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <string>\n",
    "\n",
    "template<class T>\n",
    "struct PrintSquareHelper<T, std::string>\n",
    "{\n",
    "    \n",
    "    static void Do(T t, std::string u)\n",
    "    {\n",
    "        std::cout << \"Specialized instantiation\" << std::endl;\n",
    "        std::cout << \"(t^2, u^2) = (\" << t * t << \", \" << (u + u) << \")\" << std::endl;\n",
    "    }\n",
    "    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<class T, class U>\n",
    "void PrintSquare2(T t, U u)\n",
    "{\n",
    "    PrintSquareHelper<T, U>::Do(t, u);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    PrintSquare2<double, std::string>(5., std::string(\"Hello\"));\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### If constexpr\n",
    "\n",
    "C++ 17 introduced (at last!) a special `if` that is explicitly checked at compile time.\n",
    "\n",
    "This alleviates greatly the codes and is really something I yearned for years:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "#include <string>\n",
    "\n",
    "template<class T, class U>\n",
    "void Print3(T t, U u)\n",
    "{\n",
    "    if constexpr (std::is_same<U, std::string>()) // We will see STL algorithms later\n",
    "    {\n",
    "        std::cout << \"Specialized instantiation\" << std::endl;\n",
    "        std::cout << \"(t^2, u^2) = (\" << t * t << \", \" << (u + u) << \")\" << std::endl;        \n",
    "    }\n",
    "    else\n",
    "    {\n",
    "        std::cout << \"Generic  instantiation\" << std::endl;\n",
    "        std::cout << \"(t^2, u^2) = (\" << t * t << \", \" << u * u << \")\" << std::endl;        \n",
    "    }\n",
    "}\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Print3<double, std::string>(5., \"hello\");\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note: and without constexpr?**\n",
    "\n",
    "I haven't specified it so far, but the `constexpr` is crucial: without it the compiler must instantiate both branches, even if only one is kept at the end. And if one can't be instantiated we're screwed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "template<class T, class U>\n",
    "void Print4(T t, U u)\n",
    "{\n",
    "    if (std::is_same<U, std::string>())\n",
    "    {\n",
    "        std::cout << \"Specialized instantiation\" << std::endl;\n",
    "        std::cout << \"(t^2, u^2) = (\" << t * t << \", \" << (u + u) << \")\" << std::endl;        \n",
    "    }\n",
    "    else\n",
    "    {\n",
    "        std::cout << \"Generic  instantiation\" << std::endl;\n",
    "        std::cout << \"(t^2, u^2) = (\" << t * t << \", \" << u * u << \")\" << std::endl;        \n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Print4<double, std::string>(5., \"Hello\"); // Compilation error!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "[© Copyright](../COPYRIGHT.md)   \n",
    "",
    ""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
