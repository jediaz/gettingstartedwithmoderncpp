{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](./) - [Templates](./0-main.ipynb) - [Hints to more advanced concepts with templates](./5-MoreAdvanced.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Curiously-recurrent-template-pattern-(CRTP)\" data-toc-modified-id=\"Curiously-recurrent-template-pattern-(CRTP)-1\">Curiously recurrent template pattern (CRTP)</a></span></li><li><span><a href=\"#Traits\" data-toc-modified-id=\"Traits-2\">Traits</a></span></li><li><span><a href=\"#Policies\" data-toc-modified-id=\"Policies-3\">Policies</a></span></li><li><span><a href=\"#Variadic-templates\" data-toc-modified-id=\"Variadic-templates-4\">Variadic templates</a></span></li><li><span><a href=\"#Template-template-parameters-(not-a-mistake...)\" data-toc-modified-id=\"Template-template-parameters-(not-a-mistake...)-5\">Template template parameters (not a mistake...)</a></span></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have barely scratched the surface of what can be done with templates; I will here just drop few names and a very brief explanation to allow you to dig deeper if it might seem of interest for your codes (a Google search for either of them will give you plenty of references) and also avoid you frowing upon a seemingly daunting syntax...\n",
    "\n",
    "## Curiously recurrent template pattern (CRTP)\n",
    "\n",
    "One of my own favourite idiom (so much I didn't resist writing an [entry](../7-Appendix/Crtp.ipynb) about it in the appendix).\n",
    "\n",
    "The idea behind it is to provide a same set of a given functionality to classes that have otherwise nothing in common.\n",
    "\n",
    "The basic example is if you want to assign a unique identifier to a class of yours: the implementation would be exactly the same in each otherwise different class in which you need this:\n",
    "\n",
    "* Initializing properly this identifier at construction.\n",
    "* Check no other objects of the same class use it already.\n",
    "* Provide an accessor `GetUniqueIdentifier()`.\n",
    "\n",
    "Usual inheritance or composition aren't very appropriate to put in common once and for all (DRY principle!) these functionalities: either they may prove dangerous (inheritance) or be very wordy (composition).\n",
    "\n",
    "The **curiously recurrent template pattern** is a very specific inheritance:\n",
    "\n",
    "````class MyClass : public UniqueIdentifier<MyClass>````\n",
    "\n",
    "where your class inherits from a template class which parameter is... your class itself."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Traits\n",
    "\n",
    "A **trait** is a member of a class which gives exclusively an information about type. For instance let's go back to the `HoldAValue` class we wrote [earlier](./2-Specialization.ipynb) in our template presentation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "#include <string>\n",
    "\n",
    "template<class T>\n",
    "class HoldAValue\n",
    "{\n",
    "    public:\n",
    "    \n",
    "        HoldAValue(T value);\n",
    "        \n",
    "        T GetValue() const;\n",
    "        \n",
    "    private:\n",
    "    \n",
    "        T value_;    \n",
    "};\n",
    "\n",
    "\n",
    "template<class T>\n",
    "HoldAValue<T>::HoldAValue(T value)\n",
    ": value_(value)\n",
    "{ }\n",
    "\n",
    "\n",
    "template<class T>\n",
    "T HoldAValue<T>::GetValue() const\n",
    "{ \n",
    "    return value_;    \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    HoldAValue<int> hint(5);   \n",
    "    std::cout << hint.GetValue() << std::endl;\n",
    "    \n",
    "    HoldAValue<std::string> sint(\"Hello world!\");\n",
    "    std::cout << sint.GetValue() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This class was not especially efficient: the accessor `GetValue()`:\n",
    "\n",
    "- Requires `T` is copyable.\n",
    "- Copy `T`, which is potentially a time-consuming operator. \n",
    "\n",
    "We could replace by `const T& GetValue() const` to solve both those issues, but it's a bit on the nose (and less efficient) for plain old data type. The best of both world may be achieved by a trait:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "#include <string>\n",
    "#include <type_traits> // for std::conditional, std::is_trivial\n",
    "\n",
    "\n",
    "template<class T>\n",
    "class ImprovedHoldAValue\n",
    "{\n",
    "    public:\n",
    "        \n",
    "        // Traits: information about type!\n",
    "        using return_value = \n",
    "            typename std::conditional<std::is_trivial<T>::value, T, const T&>::type;\n",
    "    \n",
    "        ImprovedHoldAValue(T value);\n",
    "        \n",
    "        return_value GetValue() const;\n",
    "        \n",
    "    private:\n",
    "    \n",
    "        T value_;    \n",
    "};\n",
    "\n",
    "\n",
    "template<class T>\n",
    "ImprovedHoldAValue<T>::ImprovedHoldAValue(T value)\n",
    ": value_(value)\n",
    "{ }\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Beware the trait that acts as the return value must be scoped correctly in the definition:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<class T>\n",
    "typename ImprovedHoldAValue<T>::return_value ImprovedHoldAValue<T>::GetValue() const\n",
    "{ \n",
    "    return value_;    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(unless you use the alternate syntax for function):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Doesn't work in Xeus-cling but completely valid in a real environment\n",
    "template<class T>\n",
    "auto ImprovedHoldAValue<T>::GetValue() const -> return_value\n",
    "{ \n",
    "    return value_;    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And the result remain the same, albeit more efficient as a copy is avoided:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{    \n",
    "    ImprovedHoldAValue<int> hint(5);   \n",
    "    std::cout << hint.GetValue() << std::endl;\n",
    "    \n",
    "    ImprovedHoldAValue<std::string> sint(\"Hello world!\");\n",
    "    std::cout << sint.GetValue() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A more complete example with an uncopyable class and the proof the alternate syntax works is available [@Coliru](https://coliru.stacked-crooked.com/a/698eb583b5a94bc7).\n",
    "\n",
    "In fact sometimes you may even have **traits class**: class which sole purpose is to provide type information! Such classes are often used as template parameters of other classes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Policies\n",
    "\n",
    "Policies are a way to provide a class for which a given aspect is entirely configurable by another class you provide as a template parameter.\n",
    "\n",
    "STL uses up policies: for instance there is a second optional template parameter to `std::vector` which deals with the way to allocate the memory (and only with that aspect). So you may provide your own way to allocate the memory and provide it to `std::vector`, which will use it instead of its default behaviour. \\cite{Alexandrescu2001} dedicates a whole chapter of his book to this example: he wrote an allocator aimed at being more efficient for the allocation of small objects.\n",
    "\n",
    "The syntax of a policy is a template class which also derives from at least one of its template parameter:\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<class ColorPolicyT>\n",
    "class Car : public ColorPolicyT\n",
    "{ };"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "struct Blue\n",
    "{\n",
    "    \n",
    "    void Print() const\n",
    "    {\n",
    "        std::cout << \"My color is blue!\" << std::endl;\n",
    "    }\n",
    "    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <string>\n",
    "\n",
    "// Let's assume in the future a car provides a mechanism to change its color at will:\n",
    "class Changing\n",
    "{\n",
    "    public:\n",
    "    void Display() const // I do not use `Print()` intentionally to illustrate there is no constraint\n",
    "                         // but in true code it would be wise to use same naming scheme!\n",
    "    {\n",
    "        std::cout << \"Current color is \" << color_ << \"!\" << std::endl;\n",
    "    }\n",
    "    \n",
    "    void ChangeColor(const std::string& new_color)\n",
    "    {\n",
    "        color_ = new_color;\n",
    "    }\n",
    "    \n",
    "    private:\n",
    "    \n",
    "        std::string color_ = \"white\"; \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    Car<Blue> blue_car;    \n",
    "    blue_car.Print();\n",
    "    \n",
    "    Car<Changing> future_car;\n",
    "    future_car.Display();\n",
    "    future_car.ChangeColor(\"black\");\n",
    "    future_car.Display();\n",
    "    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Variadic templates\n",
    "\n",
    "If you have already written some C, you must be familiar with `printf`:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <cstdio>\n",
    "\n",
    "{\n",
    "    int i = 5;\n",
    "    double d = 3.1415;\n",
    "    \n",
    "    printf(\"i = %d\\n\", i);\n",
    "    printf(\"i = %d and d = %lf\\n\", i, d);        \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This function is atypical as it may take an arbitrary number of arguments. You can devise similar function of your own in C (look for `va_arg` if you insist...) but it was not recommended: under the hood it is quite messy, and limits greatly the checks your compiler may perform on your code (especially regarding the type of the arguments).\n",
    "\n",
    "C++ 11 introduced **variadic templates**, which provides a much neater way to provide this kind of functionality (albeit with a *very* tricky syntax: check all the `...` below... and it becomes worse if you need to propagate them)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "// Overload when one value only.\n",
    "template<class T>\n",
    "void Print(T value)\n",
    "{\n",
    "    std::cout << value << std::endl;\n",
    "}\n",
    "\n",
    "// Overload with a variadic number of arguments\n",
    "template<class T, class ...Args>\n",
    "void Print(T value, Args... args)  // args here will be all parameters passed to the function from the \n",
    "                                   // second one onward.\n",
    "{\n",
    "    Print(value);\n",
    "    Print(args...); // Will call recursively `Print()` with one less argument.\n",
    "}\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Print(5, \"hello\", \"world\", \"ljksfo\", 3.12);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Print(\"One\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Print(); // Compilation error: no arguments isn't accepted!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To learn more about them, I recommend \\cite{Meyers2015}, which provides healthy explanations about `std::forward` and `std::move` you will probably need soon if you want to use these variadic templates."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Template template parameters (not a mistake...)\n",
    "\n",
    "You may want to be way more specific when defining a template parameter: instead of telling it might be whatever you want, you may impose that a specific template parameter should only be a type which is itself an instantiation of a template.\n",
    "\n",
    "Let's consider a very dumb template function which purpose is to call print the value of `size()` for a STL container. We'll see them more extensively in a [dedicated notebook](../5-UsefulConceptsAndSTL/3-Containers.ipynb), but for now you just have to know that these containers take two template parameters:\n",
    "\n",
    "- One that describe the type inside the container (e.g. `double` for `std::vector<double>`).\n",
    "- Another optional one which specifies how the memory is allocated.\n",
    "\n",
    "We could not bother and use directly a usual template parameter:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream> \n",
    "\n",
    "template<class ContainerT>\n",
    "void PrintSize1(const ContainerT& container)\n",
    "{\n",
    "    std::cout << container.size() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may use it on seamlessly on usual STL containers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "#include <list>\n",
    "#include <deque>\n",
    "\n",
    "{\n",
    "    std::vector<double> vector { 3.54, -73.1, 1004. };\n",
    "    \n",
    "    std::list<int> list { 15, -87, 12, 12, 0, -445 };\n",
    "    \n",
    "    std::deque<unsigned int> deque { 2, 87, 95, 14, 451, 10, 100, 1000 };\n",
    "    \n",
    "    \n",
    "    PrintSize1(vector);\n",
    "    PrintSize1(list);    \n",
    "    PrintSize1(deque);    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, it would also work for any class that define a `size()` parameters, regardless of its nature."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <string>\n",
    "\n",
    "struct NonTemplateClass\n",
    "{\n",
    "    std::string size() const\n",
    "    {\n",
    "        return \"Might seem idiotic, but why not?\";\n",
    "    }\n",
    "        \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "template<class U, class V, class W>\n",
    "struct TemplateWithThreeParameters\n",
    "{\n",
    "  \n",
    "    int size() const\n",
    "    {\n",
    "        return -99;\n",
    "    }\n",
    "    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    NonTemplateClass non_template_class;\n",
    "    TemplateWithThreeParameters<int, float, double> template_with_three_parameters;\n",
    "    \n",
    "    PrintSize1(non_template_class);\n",
    "    PrintSize1(template_with_three_parameters);\n",
    "    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see here with my rather dumb example that `PrintSize1()` also works for my own defined types, that are not following the expected prototype of a STL container (class with two template parameters). \n",
    "\n",
    "It may seem pointless in this example, but the worst is that the method might be used to represent something entirely different from what we expect when we call `size()` upon a STL container.\n",
    "\n",
    "A possibility to limit the risk is to use a **template template parameter** in the function definition:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "template<template <class, class> class ContainerT, class TypeT, class AllocatorT>\n",
    "void PrintSize2(const ContainerT<TypeT, AllocatorT>& container)\n",
    "{\n",
    "    std::cout << container.size() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By doing so, we impose that the type of the argument is an instantiation of a class with two arguments. With that, STL containers work:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "#include <list>\n",
    "#include <deque>\n",
    "\n",
    "{\n",
    "    std::vector<double> vector { 3.54, -73.1, 1004. };\n",
    "    \n",
    "    std::list<int> list { 15, -87, 12, 12, 0, -445 };\n",
    "    \n",
    "    std::deque<unsigned int> deque { 2, 87, 95, 14, 451, 10, 100, 1000 };\n",
    "    \n",
    "    // At call site, you don't have to specify the template arguments that are inferred.\n",
    "    PrintSize2(vector);\n",
    "    PrintSize2(list);    \n",
    "    PrintSize2(deque);    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "whereas my own defined types don't:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    NonTemplateClass non_template_class;\n",
    "    TemplateWithThreeParameters<int, float, double> template_with_three_parameters;\n",
    "    \n",
    "    PrintSize2(non_template_class);\n",
    "    PrintSize2(template_with_three_parameters);\n",
    "    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In practice you shouldn't need to use that too often, but as the topics in this notebook it is worthy to know the possibility exists (that may help you understand an error message should you use a library using them). I had to resort to them a couple of times, especially along policies.\n",
    "\n",
    "If you want to learn more about them, you should really read \\cite{Alexandrescu2001})."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# References\n",
    "\n",
    "(<a id=\"cit-Alexandrescu2001\" href=\"#call-Alexandrescu2001\">Alexandrescu, 2001</a>) Andrei Alexandrescu, ``_Modern C++ Design: Generic Programming and Design Patterns applied_'', 01 2001.\n",
    "\n",
    "(<a id=\"cit-Meyers2015\" href=\"#call-Meyers2015\">Meyers, 2015</a>) Scott Meyers, ``_Effective modern C++: 42 specific ways to improve your use of C++11\n",
    "               and C++14_'',  2015.  [online](http://www.worldcat.org/oclc/890021237)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "[© Copyright](../COPYRIGHT.md)   \n",
    "",
    ""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
