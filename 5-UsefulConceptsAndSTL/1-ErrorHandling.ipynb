{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](./) - [Useful concepts and STL](./0-main.ipynb) - [Error handling](./1-ErrorHandling.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Introduction\" data-toc-modified-id=\"Introduction-1\">Introduction</a></span></li><li><span><a href=\"#Compiler-warnings-and-errors\" data-toc-modified-id=\"Compiler-warnings-and-errors-2\">Compiler warnings and errors</a></span></li><li><span><a href=\"#Assert\" data-toc-modified-id=\"Assert-3\">Assert</a></span></li><li><span><a href=\"#Exceptions\" data-toc-modified-id=\"Exceptions-4\">Exceptions</a></span><ul class=\"toc-item\"><li><span><a href=\"#throw\" data-toc-modified-id=\"throw-4.1\"><code>throw</code></a></span></li><li><span><a href=\"#try/catch\" data-toc-modified-id=\"try/catch-4.2\"><code>try</code>/<code>catch</code></a></span></li><li><span><a href=\"#Re-throw\" data-toc-modified-id=\"Re-throw-4.3\">Re-throw</a></span></li><li><span><a href=\"#Good-practice:-use-as-much-as-possible-exceptions-that-derive-from-std::exception\" data-toc-modified-id=\"Good-practice:-use-as-much-as-possible-exceptions-that-derive-from-std::exception-4.4\">Good practice: use as much as possible exceptions that derive from <code>std::exception</code></a></span></li><li><span><a href=\"#Good-practice:-be-wary-of-a-forest-of-exception-classes\" data-toc-modified-id=\"Good-practice:-be-wary-of-a-forest-of-exception-classes-4.5\">Good practice: be wary of a forest of exception classes</a></span></li><li><span><a href=\"#noexcept\" data-toc-modified-id=\"noexcept-4.6\"><code>noexcept</code></a></span></li><li><span><a href=\"#Good-practice:-never-throw-an-exception-from-a-destructor\" data-toc-modified-id=\"Good-practice:-never-throw-an-exception-from-a-destructor-4.7\">Good practice: never throw an exception from a destructor</a></span></li><li><span><a href=\"#The-exception-class-I-use\" data-toc-modified-id=\"The-exception-class-I-use-4.8\">The exception class I use</a></span></li></ul></li><li><span><a href=\"#Error-codes\" data-toc-modified-id=\"Error-codes-5\">Error codes</a></span><ul class=\"toc-item\"><li><span><a href=\"#nodiscard\" data-toc-modified-id=\"nodiscard-5.1\">nodiscard</a></span></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "It is very important of course to be able to track and manage as nicely as possible when something goes south in your code. We will see in this chapter the main ways to provide such insurance.\n",
    "\n",
    "## Compiler warnings and errors\n",
    "\n",
    "The first way to find out possible errors are during compilation time: you may ensure your code is correct by making its compilation fails if not (that's exactly the spirit of the example we provided for [template template parameter](../4-Templates/5-MoreAdvanced.ipynb#Template-template-parameters-(not-a-mistake...)). There are many ways to do so, even more so if templates are involved; here are few of them we have already seen:\n",
    "\n",
    "* `static_assert` we saw in [template introduction](../4-Templates/1-Intro.ipynb#static_assert)\n",
    "* Duck typing failure: if a template argument used somewhere doesn't comply with the expected API.\n",
    "* Locality of reference: use heavily blocks so that a variable is freed as soon as possible. This way, you will avoid mistakes of using a variable that is in fact no longer up-to-date.\n",
    "* Strive to make your code without any compiler warning: if there are even as less as 10 warnings, you might not see an eleventh that might sneak its way at some point. Activate as many types of warnings as possible for your compiler, and deactivate those unwanted with care (see [this notebook](../6-InRealEnvironment/4-ThirdParty.ipynb) to see how to manage third party warnings.).\n",
    "\n",
    "## Assert\n",
    "\n",
    "`assert` is a very handy tool that your code behaves exactly as expected. `assert` takes one argument; if this argument is resolved to `false` the code aborts with an error message:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#undef NDEBUG // Don't bother with this outside of Xeus-cling!\n",
    "#include <cassert>\n",
    "#include <iostream>\n",
    "\n",
    "// THIS CODE WILL KILL Xeus-cling kernel!\n",
    "{\n",
    "    double* ptr = nullptr;\n",
    "    assert(ptr != nullptr && \"Pointer should be initialized first!\"); \n",
    "    //< See the `&&`trick above: you can't provide a message like in `static_assert`,\n",
    "    // but you may use a AND condition and a string to detail the issue\n",
    "    // (it works because the string is evaluated as `true`).\n",
    "    std::cout << *ptr << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(here in Xeus-cling it breaks the kernel...)\n",
    "\n",
    "The perk of `assert` is that it checks the condition is `true` *only in debug mode*! \n",
    "\n",
    "So you may get extensive tests in debug mode that are ignored once your code has been thoroughly checked and is ready for production use (_debug_ and _release_ mode will be explained in a [later notebook](../6-InRealEnvironment/3-Compilers.ipynb#Debug-and-release-flags)).\n",
    "\n",
    "The example above is a very useful use: before dereferencing a pointer checks it is not `nullptr` (hence the good practice to always initialize a pointer to `nullptr`...)\n",
    "\n",
    "In **release mode**, the macro `NDEBUG` should be defined and all the `assert` declarations will be ignored by the compiler.\n",
    "\n",
    "I recommend to use `assert` extensively in your code:\n",
    "\n",
    "* You're using a pointer? Check it is not `nullptr`.\n",
    "* You get in a function a `std::vector` and you know it should be exactly 3 elements long? Fire up an assert to check this...\n",
    "* A `std::vector` is expected to be sorted a given way? Check it through an `assert`... (yes it's a O(n) operation, but if your contract is broken you need to know it!)\n",
    "\n",
    "Of course, your debug mode will be much slower; but its role is anyway to make sure your code is correct, not to be the fastest possible (release mode is there for that!)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exceptions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Asserts are clearly a **developer** tool: they are there to signal something does not behave as intended and therefore that there is a bug somewhere...\n",
    "\n",
    "However, they are clearly not appropriate to handle an error of your program end-user: for instance if he specifies an invalid input file, you do not want an `abort` which is moreover handled only in debug mode!\n",
    "\n",
    "### `throw`\n",
    "\n",
    "There is an **exception** mechanism that is appropriate to deal with this; this mechanism is activated with the keyword `throw`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void FunctionThatExpectsSingleDigitNumber(int n)\n",
    "{\n",
    "    if (n < -9)\n",
    "        throw -1;\n",
    "    \n",
    "    if (n > 9)\n",
    "        throw 1;\n",
    "    \n",
    "    std::cout << \"Valid digit is \" << n << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    FunctionThatExpectsSingleDigitNumber(5);\n",
    "    std::cout << \"End\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    FunctionThatExpectsSingleDigitNumber(15);\n",
    "    std::cout << \"End\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, an exception provokes an early exit of the function: the lines after the exception is thrown are not run, and unless it is caught it will stop at the abortion of the program."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `try`/`catch`\n",
    "\n",
    "`throw` expects an object which might be intercepted by the `catch` command if the exception occurred in a `try` block; `catch` is followed by a block in which something may be attempted (or not!)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    try\n",
    "    { \n",
    "        FunctionThatExpectsSingleDigitNumber(15);\n",
    "    }\n",
    "    catch(int n)\n",
    "    {\n",
    "        if (n == 1)\n",
    "            std::cerr << \"Error: value is bigger than 9!\" << std::endl;\n",
    "        if (n == -1)\n",
    "            std::cerr << \"Error: value is less than -9!\" << std::endl;\n",
    "    }\n",
    "    std::cout << \"End\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the type doesn't match, the exception is not caught; if you want to be sure to catch everything you may use the `...` syntax. The drawback with this syntax is you can't use the thrown object information:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    try\n",
    "    { \n",
    "        FunctionThatExpectsSingleDigitNumber(15);\n",
    "    }\n",
    "    catch(float n) // doesn't catch your `int` exception!\n",
    "    {\n",
    "        std::cerr << \"Float case: \" << n << \" was provided and is not an integer\" << std::endl;\n",
    "    }\n",
    "    catch(...)\n",
    "    {\n",
    "        std::cerr << \"Gluttony case... but no object to manipulate to extract more information!\" << std::endl;\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Re-throw\n",
    "\n",
    "Once an exception has been caught by a `catch` block, it is considered to be handled; the code will therefore go on to what is immediately after the block. If you want to throw the exception again (for instance after logging a message) you may just type `throw`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// No re-throw\n",
    "{\n",
    "    try\n",
    "    { \n",
    "        FunctionThatExpectsSingleDigitNumber(15);\n",
    "    }\n",
    "    catch(int n)\n",
    "    {\n",
    "        std::cerr << \"Int case: \" << n << \" not a single digit number\" << std::endl;\n",
    "    }\n",
    "    \n",
    "    std::cout << \"After catch\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Rethrow\n",
    "{\n",
    "    try\n",
    "    { \n",
    "        FunctionThatExpectsSingleDigitNumber(15);\n",
    "    }\n",
    "    catch(int n)\n",
    "    {\n",
    "        std::cerr << \"Int case: \" << n << \" not a single digit number\" << std::endl;\n",
    "        throw; // `throw n` would have been correct as well but is not necessary\n",
    "    }\n",
    "    \n",
    "    std::cout << \"After catch\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Good practice: use as much as possible exceptions that derive from `std::exception`\n",
    "\n",
    "Using the _catch all_ case is not recommended in most cases... In fact even the `int`/`float` case is not that smart: it is better to use an object with information about why the exception was raised in the first place.\n",
    "\n",
    "It is advised to use exception classes derived from the `std::exception` one; this way you provide a catch all without the drawback mentioned earlier. This class provides a virtual `what()` method which gives away more intel about the issue:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <exception>\n",
    "\n",
    "struct TooSmallError : public std::exception\n",
    "{\n",
    "    virtual const char* what() const noexcept override // we'll go back to `noexcept` later...\n",
    "    {\n",
    "        return \"Value is less than -9!\";\n",
    "    }\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "struct TooBigError : public std::exception\n",
    "{\n",
    "    virtual const char* what() const noexcept override\n",
    "    {\n",
    "        return \"Value is more than 9!\";\n",
    "    }\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void FunctionThatExpectsSingleDigitNumber2(int n)\n",
    "{\n",
    "    if (n < -9)\n",
    "        throw TooSmallError();\n",
    "    \n",
    "    if (n > 9)\n",
    "        throw TooBigError();\n",
    "    \n",
    "    std::cout << \"Valid digit is \" << n << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    try\n",
    "    { \n",
    "        FunctionThatExpectsSingleDigitNumber2(15);\n",
    "    }\n",
    "    catch(const std::exception& e)\n",
    "    {\n",
    "        std::cerr << \"Properly caught: \" << e.what() << std::endl;\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The information is now in the exception object, which is much better...\n",
    "\n",
    "Unfortunately, you are not always privy to the choice: if for instance you're using [Boost library](https://www.boost.org) the exception class they use don't inherit from `std::exception` (but some derived ones such as `boost::filesystem::error` do...). In this case, make sure to foresee to catch them with a dedicated block:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "// Pseudo-code - Do not run in Xeus-cling!\n",
    "\n",
    "try\n",
    "{\n",
    "    ...    \n",
    "}\n",
    "catch(const std::exception& e)\n",
    "{\n",
    "    ...\n",
    "}\n",
    "catch(const boost::exception& e)\n",
    "{\n",
    "    ...\n",
    "}\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Good practice: be wary of a forest of exception classes\n",
    "\n",
    "At first sight, it might be tempting to provide a specific exception whenever you want to throw one: this way, you are able to catch only this one later on.\n",
    "\n",
    "In practice, it's not necessarily such a good idea:\n",
    "\n",
    "* When the code becomes huge, you (and even more importantly a new developer) may be lost in all the possible exceptions.\n",
    "* It is rather time consuming: defining a specific exception means a bit of boilerplate to write, and those minutes might have been spent more efficiently, as...\n",
    "* Most of the time, you don't even need the filtering capacity; in my code for instance if an exception is thrown it is 99 % of the time to be caught in the `main()` function to terminate properly the execution.\n",
    "\n",
    "The only case in which it might be very valuable to use a tailored exception is for your integration tests: if you are writing a test in which an exception is expected, it is better to check the exception you caught is exactly the one that was expected and not a completely unrelated exception which was thrown for another reason.\n",
    "\n",
    "STL provides many derived class from `std::exception` which you might use directly or as base of your own class; see [cppreference](https://en.cppreference.com/w/cpp/error/exception) for more details. [OpenClassrooms](https://openclassrooms.com/fr/courses/1894236-programmez-avec-le-langage-c/1903837-gerez-des-erreurs-avec-les-exceptions) (which despite its name is in french) sorted out the go-to exceptions for lazy developers which cover most of the cases (don't get me wrong: laziness is often an asset for a software developer!):\n",
    "\n",
    "* `std::domain_error`\n",
    "* `std::invalid_argument`\n",
    "* `std::length_error`\n",
    "* `std::out_of_range`\n",
    "* `std::logic_error`\n",
    "* `std::range_error`\n",
    "* `std::overflow_error`\n",
    "* `std::underflow_error`\n",
    "* `std::runtime_error`\n",
    "\t\n",
    "with the latter being the default choice if no other fit your issue. Most of those classes provide a `std::string` argument in its constructor so that you may explain exactly what went wrong.\n",
    "\n",
    "\n",
    "### `noexcept`\n",
    "\n",
    "Exceptions are in fact very subtle to use; see for instance \\cite{Sutter1999} and \\cite{Sutter2002} that deal with them extensively (hence their title!).\n",
    "\n",
    "In C++03, it was possible to specify a method or a function wouldn't throw, but the underlying mechanism with keywords `throw` and `nothrow` was such a mess many C++ gurus warned against using them.\n",
    "\n",
    "In C++11, they tried to rationalize it and a new keyword to replace them was introduced: `noexcept`. \n",
    "\n",
    "In short, if you have a method you're 100 % percent sure can't throw an exception, add this suffix and the compiler may optimize even further. However, do not put it if an exception can be thrown: it would result in a ugly runtime crash should an exception be raised there... (and up to now compilers are completely oblivious to that: no associated warning is displayed).\n",
    "\n",
    "As you saw, in recent C++ `what()` is to be a `noexcept` method. It is therefore a bad idea to try to allocate there the string to be returned: allocation and string manipulation could lead to an exception from the STL functions used."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Good practice: never throw an exception from a destructor\n",
    "\n",
    "The explanation is quite subtle and explained in detail in item 8 of \\cite{Meyers2005}; however just know you should never throw an exception there. If you need to deal with an error there, use something else (`std::abort` for instance).\n",
    "\n",
    "\n",
    "### The exception class I use\n",
    "\n",
    "I (Sébastien) provide in [appendix](/notebooks/7-Appendix/HomemadeException.ipynb) my own exception class (which of course derives from `std::exception`) which provides additionally:\n",
    "\n",
    "* A constructor with a string, to avoid defining a verbosy dedicated exception class for each case.\n",
    "* Better management of the string display, with an underlying `std::string` object.\n",
    "* Information about the location from where the exception was thrown.\n",
    "\n",
    "Vincent uses up the STL exceptions described in [previous section](#Good-practice:-be-wary-of-a-forest-of-exception-classes).\n",
    "\n",
    "## Error codes\n",
    "\n",
    "A quick word about C-style error management which you might find in use in some libraries: **error codes**.\n",
    "\n",
    "The principe of the error codes is that your functions and methods should return an `int` which provides an indication of the success or not of the call; the eventual values sought are returned from reference. For instance:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <type_traits>\n",
    "\n",
    "constexpr auto INVALID_TYPE = -1;\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "template<class T>\n",
    "int AbsoluteValue(T value, T& result)\n",
    "{\n",
    "    if constexpr (!std::is_arithmetic<T>())\n",
    "        return INVALID_TYPE;\n",
    "    else\n",
    "    {\n",
    "        if (value < 0)\n",
    "            result = -value;\n",
    "        else\n",
    "            result = value;\n",
    "\n",
    "        return EXIT_SUCCESS;\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I don't like these error codes much, because:\n",
    "\n",
    "* The result can't be naturally given in return value and must be provided in argument.\n",
    "* You have to bookkeep the possible error codes somewhere, and a user must know this somewhere to go consult them if something happens (usually a header file: see for instance one for [PETSc library](https://www.mcs.anl.gov/petsc/petsc-master/include/petscerror.h.html)).\n",
    "* In the libraries that use them, more often than not some are not self descriptive and you have to figure out what the hell the issue is.\n",
    "* And more importantly, this relies on the end-user thinking to check the error value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <string>\n",
    "\n",
    "{\n",
    "    std::string hello(\"Hello world\");\n",
    "    std::string absolute_value = \"\";\n",
    "    \n",
    "    AbsoluteValue(hello, absolute_value); // No compilation or runtime error (or even warning)!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It should be noticed C++ 11 introduced a dedicated class to handle more gracefully error codes: [`std::error_code`](https://en.cppreference.com/w/cpp/error/error_code). I have no direct experience with it but it looks promising as illustrated by this [blog post](https://akrzemi1.wordpress.com/2017/07/12/your-own-error-code/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### nodiscard\n",
    "\n",
    "The point about forgetting to check the value may however be mitigated since C++17 with the attribute [``nodiscard``](https://en.cppreference.com/w/cpp/language/attributes/nodiscard), which helps your compiler figure out the return value should have been checked."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "template<class T>\n",
    "[[nodiscard]] int AbsoluteValueNoDiscard(T value, T& result)\n",
    "{\n",
    "    if constexpr (!std::is_arithmetic<T>())\n",
    "        return INVALID_TYPE;\n",
    "    else\n",
    "    {\n",
    "        if (value < 0)\n",
    "            result = -value;\n",
    "        else\n",
    "            result = value;\n",
    "\n",
    "        return EXIT_SUCCESS;\n",
    "    }\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <string>\n",
    "\n",
    "{\n",
    "    std::string hello(\"Hello world\");\n",
    "    std::string absolute_value = \"\";\n",
    "    \n",
    "    AbsoluteValueNoDiscard(hello, absolute_value); // Now there is a warning! But only available after C++ 17...\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# References\n",
    "\n",
    "[<a id=\"cit-Sutter1999\" href=\"#call-Sutter1999\">Sutter1999</a>] Herb Sutter, ``_Exceptional C++: 47 Engineering Puzzles, Programming Problems, and Solutions_'',  2000.\n",
    "\n",
    "[<a id=\"cit-Sutter2002\" href=\"#call-Sutter2002\">Sutter2002</a>] Herb Sutter, ``_More Exceptional C++: 40 New Engineering Puzzles, Programming Problems, and Solutions_'',  2002.\n",
    "\n",
    "[<a id=\"cit-Meyers2005\" href=\"#call-Meyers2005\">Meyers2005</a>] Scott Meyers, ``_Effective C++: 55 Specific Ways to Improve Your Programs and Designs (3rd Edition)_'',  2005.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "[© Copyright](../COPYRIGHT.md)   \n",
    "",
    ""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "key",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "279px"
   },
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
