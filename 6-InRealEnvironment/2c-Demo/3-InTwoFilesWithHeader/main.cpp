#include <cstdlib> // for EXIT_SUCCESS
#include "hello.hpp"

int main(int argc, char** argv)
{
    hello();
    
    return EXIT_SUCCESS;
}