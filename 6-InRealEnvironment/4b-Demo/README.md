# How to use Docker for this example?

The image is based on Fedora and is created through this [Dockerfile](../docker/Dockerfile.boost). 

To run it type:

````
docker run -it -v $PWD:/home/formation/gettingstartedwithmoderncpp --cap-drop=all registry.gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/with_boost:latest
````

# Step by step: illustrating the issue

After run, the container is opened and you are in a folder (inside the container) named `/Codes/ThirdPartyWarning`.

**NOTE** All the commands listed below must be run _INSIDE_ the container.

## Illustrating the issue

Go in the first directory:

````
cd 1-ProblematicCase/
````

You will see there the `simple_boost.cpp` presented in the notebook, and a CMakeLists.txt to compile it with CMake.

Please proceed:

````
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
````

At the time of this writing, I get 1355 warnings! (more than in the first run one year ago with Boost 1.69...)

## First solution: system directories

Go in the second directory:

````
cd ../../2-FixByBuildSystem
````

The only change is in the CMakeLists.txt file: 

````
target_include_directories(simple_boost PUBLIC "/Codes/ThirdParty/opt/include")
````

is replaced by:

````
target_include_directories(simple_boost SYSTEM PUBLIC "/Codes/ThirdParty/opt/include")
````

And now the compilation works without warnings (from third party at least!):

````
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
````

## Second solution: pragmas

### Fixing clang warnings

Go in the third directory:

````
cd ../../3-FixByPragmas-clang
````

This time, `CMakeLists.txt` is exactly the same as in `1-ProblematicCase` but the `simple_boost.cpp` is amended: includes are surrounded by clang pragmas to deactivate warnings.

````
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
````

and we're all right! At least if we stick with clang... Let's compile with gcc to see:

````
cd ..
mkdir build_gcc
cd build_gcc
cmake -G Ninja -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
````

and we get two type of warnings:

* -Wsign-conversion that were present in clang and fixed by the pragmas
* -Wunknown-pragmas as pragmas used for clang build are not recognized! So fixing for clang makes it somehwat worse for gcc.. (not completely - some ignored warnings do also exist in gcc and were therefore silenced).

### Fixing clang AND gcc warnings

Go in the fourth directory:

````
cd ../../4-FixByPragmas-clang-gcc/
````

As for the case we just saw, only the source file is modified. We add here macros to separate clearly gcc and clang cases:

````
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
cd ..
mkdir build_gcc
cd build_gcc
cmake -G Ninja -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
````

And both now works without warnings.

### Fixing clang AND gcc warnings a bit more gracefully

Go in the fifth directory:

````
cd ../../5-FixByPragmas-common/
````

There are two changes here:

* A specific header file has been devised to include properly Boost filesystem. This way, if later you need it somewhere else you may include this file directly and get the right output in just one include line.
* More a matter of taste: I used macro magic to put in common the pragma commands that may work for both gcc and clang.

````
mkdir build_clang
cd build_clang
cmake -G Ninja -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
cd ..
mkdir build_gcc
cd build_gcc
cmake -G Ninja -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_EXTENSIONS=OFF ..
ninja
````



