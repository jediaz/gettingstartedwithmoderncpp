#ifndef CLEAN_BOOST_FILESYSTEM_HPP
# define CLEAN_BOOST_FILESYSTEM_HPP

# include "pragma.hpp"

PRAGMA_DIAGNOSTIC(push) // starts the code block in which rules are modified
    
// First deactivate warning flags common to gcc and clang
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")
PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
PRAGMA_DIAGNOSTIC(ignored "-Wparentheses")
PRAGMA_DIAGNOSTIC(ignored "-Wcast-qual")
PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated")
PRAGMA_DIAGNOSTIC(ignored "-Wconversion")
PRAGMA_DIAGNOSTIC(ignored "-Wundef")
PRAGMA_DIAGNOSTIC(ignored "-Wzero-as-null-pointer-constant")

PRAGMA_DIAGNOSTIC(ignored "-Wpadded")
PRAGMA_DIAGNOSTIC(ignored "-Wnon-virtual-dtor")

// Deactivate here warning flags specific to clang
#ifdef __clang__    
    PRAGMA_DIAGNOSTIC(ignored "-Wreserved-id-macro")
    PRAGMA_DIAGNOSTIC(ignored "-Wweak-vtables")
    PRAGMA_DIAGNOSTIC(ignored "-Wundefined-func-template")    
    PRAGMA_DIAGNOSTIC(ignored "-Wc++98-compat")
    PRAGMA_DIAGNOSTIC(ignored "-Wc++98-compat-pedantic")
    PRAGMA_DIAGNOSTIC(ignored "-Wexit-time-destructors")
    PRAGMA_DIAGNOSTIC(ignored "-Winconsistent-missing-destructor-override")
    PRAGMA_DIAGNOSTIC(ignored "-Wsuggest-destructor-override")
#endif // __clang__

#include "boost/exception/diagnostic_information.hpp"
#include "boost/filesystem.hpp"

PRAGMA_DIAGNOSTIC(pop) // go back to normal rules

#endif // CLEAN_BOOST_FILESYSTEM_HPP
