Table of Contents
=================

- [Table of Contents](#table-of-contents)
  - [About this tutorial](#about-this-tutorial)
  - [Goal of this tutorial](#goal-of-this-tutorial)
  - [How to run it?](#how-to-run-it)
    - [Local installation](#local-installation)
      - [On Windows](#on-windows)
      - [On macOS](#on-macos)
    - [Docker](#docker)
    - [BinderHub](#binderhub)
  - [For maintainers](#for-maintainers)


## About this tutorial

This tutorial is heavily inspired from a C++ tutorial created by David Chamont (CNRS) that was given as a lecture with the help of Vincent Rouvreau (Inria) in 2016; latest version of this tutorial used as the basis of current one may be found [there](https://gitlab.inria.fr/FormationCpp/DebuterEnCpp).

Current version provides two major modifications:

* The tutorial is now in english.
* Jupyter notebooks using [Xeus-cling kernel](https://github.com/QuantStack/xeus-cling) are now used, thus enabling a sort of interpreted C++ which is rather helpful for teaching it (even if it is clearly not yet mature...)

I have rewritten entirely and modified heavily several chapters, but the backbone remains heavily indebted to David and Vincent and the hands-on is still very similar to the original one.

As the original tutorial, the present lecture is released under the [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](http://creativecommons.org/licenses/by-nc-sa/4.0/) licence.


## Goal of this tutorial

This is an introductory lecture to the modern way of programming C++; at the end of it you should:

* Understand the syntax and basic mechanisms of the C++ language in version 14/17.
* Know the different programming styles.
* Know of and be able to use the most useful part of the standard library.
* Be aware of many good programming practices in C++.


## How to run it?

### Local installation

As this tutorial relies heavily on [Xeus-cling kernel](https://github.com/QuantStack/xeus-cling), the only requirement is to install this environment on your machine.

It can be done easily (at least on Linux - see below for Windows and macOS) through:

Should the procedure described below not work at some point I invite you to check the link above, but at the time of this writing you need to:

* Install [miniconda3](https://conda.io/miniconda.html) environment (apparently using full-fledged anaconda may lead to conflict in dependencies).
* Create a new conda environment and activate it:

```
conda env create -f environment.yml
conda activate gettingstartedwithmoderncpp
```

Don't forget to activate it each time you intend to run the lecture!

* Then you can run the notebook by going **into its root directory** (or internal links won't work...) in a terminal and typing:

```
jupyter notebook
```

* Optionally, in the tree view go to `Nbextensions` tab, or in your notebook view go to `Edit > nbextensionsconfig` and:

	- Untick _Disable configuration for nbextensions without explicit compatibility_. 
	- Tick _(some) LaTeX environments for Jupyter_
	- Tick _Table of Contents(2)_. 

These options will enable minor improvements (update of table of contents and proper rendering of Bibtex entries).

__NOTE__: It is possible to use the notebooks directly from some IDEs like [VSCode](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/-/wikis/home#using-notebooks-with-vscode).

#### On Windows

No package is provided for Windows for Xeus-cling package (https://github.com/jupyter-xeus/xeus-cling).

Your best option if you're using Windows 10 or 11 is probably to  [install Ubuntu in your Windows session](https://tutorials.ubuntu.com/tutorial/tutorial-ubuntu-on-windows); I haven't tried this myself.

#### On macOS

Since at least May 2021, Conda packaging is broken for macOS (11.3). A [ticket](https://github.com/jupyter-xeus/xeus-cling/issues/403) has been issued but to no avail so far despite several users chiming in or opening similar tickets.

Situation is even [more dire](https://github.com/jupyter-xeus/xeus-cling/issues/436) for ARM computers. The best for macOS user is probably to use Binder (see below).


### Docker

It is possible to execute the notebooks in a Docker container by simply typing:

````
docker run -v $PWD:/home/formation/gettingstartedwithmoderncpp -p 8888:8888 --cap-drop=all registry.gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/xeus-cling:latest
````

or 

````
docker run -v $PWD:/home/formation/gettingstartedwithmoderncpp -p 8888:8888 --cap-drop=all registry.gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/xeus-cling_with_compilers:latest
````

Few hints for those not familiar with Docker:

* `-v` creates a mapping between local folder and the /home/formation/gettingstartedwithmoderncpp folder in the container; this enables you to edit the file from your comfy local environment and see the file edited this way in the Docker container.
* `--cap-drop=all` is a safety when you're running a Docker image not built by yourself: you're essentially blocking the few remaining operations that might impact your own environment that Docker lets by default open with the run command.
* `-p` gives the port mapping between the Docker image and your local environment.

The lengthy `registry.gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/xeus-cling-and-compilers` is the name of the Docker **image**, if this image is not present locally in your environment Docker will try to fetch it from a *registry* on the Inria Gitlab. 

Then just type [http://localhost:8888/](http://localhost:8888/) in your browser to run the notebooks.

Unfortunately, Docker is no longer free to use for Windows and macOS users. You may use alternatives such as [Podman](https://podman.io/) but the mapping between the files on the host system and those in the containers is a bit more hokey.

### BinderHub

A link to a BinderHub instance is given at the top of the project page; foresee few minutes to set up properly the notebooks.


## For maintainers

Informations related to CI are [here](CI.md).
All contributions are welcome, but please read the [contribution guide](CONTRIBUTING.md) first.
